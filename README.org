#+TITLE: README.org -- Bare atmega projects
#+OPTIONS: ^:nil

* Main Goal and Background

 A small collection of coding projects for Atmel/Microchip's ATmega
 micro-controllers (mainly atmega328p).

 For each project, the micro-controller is used just bare on a
 breadboard (or soldered on a perfboard).  In a similar way,
 additional components like SPI-based EEPROMs or SRAMs (and other
 components) are used.  To simplify handling or soldering, each
 component is used in through-hole package.

 An additional/extra hardware programmer is expected, to be able to
 upload the code into the micro-controller's flash.

 The coding base is also stripped down to the minimum, and just
 leverages a Makefile (individual to each project).

* Directory structure

 - *projects/* : Each subdir holds an independent coding project
   - *01_simple_io/* : Simple I/O example, using LEDs
   - *02_spi/* : Accessing external EEPROM/SRAM chips via SPI (multiple chips!)
   - *03_uart/* : Provide an UART interface via general I/O FILEs in C code
   - *04_temperature/* : Temperature sensor node (manual version), with time stamping, EEPROM storage, and UART interface
   - *07_uart_and_isr/* : (Tryout) automatically repeated sensor measurements via interrupts (+EEPROM and +UART)
   - *08_lcd/* : (Tryout) 2-line LCD interface for (temperature) sensor node (+EEPROM and +UART)
 - *xtra_material/* : Smaller tryouts, as also direct copies of other authors [IGN]
 - *xtra_notes/* : Couple of personal notes, kept inline for convenience [IGN]

* Dependencies, building and uploading code

 - gcc-avr: GCC compiler (for AVR cross compilation)
 - avr-libc: libc for AVR
 - avrdude: Programmer/Flasher
 - make: Build tool

** Preparing basic dependencies (Debian)

 #+BEGIN_SRC shell
 apt install gcc-avr
 apt install avr-libc
 apt install avrdude
 apt install make
 #+END_SRC

** Preparing code upload/programming

 !!Disclaimer!!: each Makefile will leverage `sudo` when uploading code to
 the chip. Make sure to prepare your `sudoers` file accordingly.  You
 have been warned!

 You may configure a non-default device for `avrdude`, by
 adjusting:

 #+BEGIN_QUOTE
 ...
 AVRDUDE_PORT = /dev/ttyUSB0
 ...
 #+END_QUOTE

** Common building and uploading strategy

 Simplified building and uploading:
 #+BEGIN_SRC shell
 cd project/01_simple_io/
 make simple_io.elf.hex
 make upload # -> Will leverage `sudo avrdude`
 make clean
 #+END_SRC

** Optional Make targets

 #+BEGIN_SRC shell
 # Create an assembler listing file, for detailed code inspection
 # (uses `objdump`)
 make simple_io.lst

 # Build project-individual libraries
 make libs

 # Remove generated/build files, only for project-individual libraries
 make clean_libs
 #+END_SRC
* License information

 This code is provided under GPL-2.0. For detailed license
 description, see: COPYING.

 
 

 
