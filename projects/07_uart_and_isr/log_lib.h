/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * log_lib.h: Level-based logger library, compile-time configurable.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#ifndef __LOG_LIB_H_
#define __LOG_LIB_H_

/* TODO:

   There's a problem when *no* parameters are used!

     - See GCC documentation on Variadic Macros (6.20 Macros with a
       Variable Number of Arguments).

	 - Use " log_debug(fmt,...) printf ("DBG: "fmt , ## __VA_ARGS__);

	 - Keep in mind that this is (a) a GCC extension and (b) will
       prevent macro expension of __VA_ARGS__.  Is a macro expansion
       expected here?
   
   How to handle multi-line log messages?  Should each line be
   prepended using:

   DBG: line 1
   DBG:    something on line 2
   DBG:    something on line 3

   There should be multiple ways to make this configurable.  On the
   one hand this should be configurable for *all* modules globally
   within a program using this.  On the other hand it should be
   configurable individually to a module!

*/

#define LOG_DEBUG 3
#define LOG_INFO  2
#define LOG_WARN  1

#define LOG_LEVEL LOG_INFO

#if LOG_LEVEL >= LOG_DEBUG
#define log_debug(fmt,...) printf ("DBG: "fmt ,__VA_ARGS__);
#define log_debug_1(fmt) printf ("DBG: "fmt);
#define log_debug_P(fmt,...) printf_P (PSTR("DBG: "fmt) ,__VA_ARGS__);
#define log_debug_1_P(fmt) printf_P (PSTR("DBG: "fmt));
#else
#define log_debug(fmt,...)
#define log_debug_1(fmt)
#define log_debug_P(fmt,...)
#define log_debug_1_P(fmt)
#endif

#if LOG_LEVEL >= LOG_INFO
#define log_info(fmt,...) printf ("INFO: "fmt ,__VA_ARGS__);
#define log_info_1(fmt) printf ("INFO: "fmt );
#define log_info_P(fmt,...) printf_P (PSTR("INFO: "fmt) ,__VA_ARGS__);
#define log_info_1_P(fmt) printf_P (PSTR("INFO: "fmt));
#else
#define log_info(fmt,...)
#define log_info_1(fmt)
#define log_info_P(fmt,...)
#define log_info_1_P(fmt)

#endif

#if LOG_LEVEL >= LOG_WARN
#define log_warn(fmt,...) printf ("WARN: "fmt ,__VA_ARGS__);
#define log_warn_1(fmt) printf ("WARN: "fmt );
#define log_warn_P(fmt,...) printf_P (PSTR("WARN: "fmt) ,__VA_ARGS__);
#define log_warn_1_P(fmt) printf_P (PSTR("WARN: "fmt));
#else
#define log_warn(fmt,...)
#define log_warn_1(fmt)
#define log_warn_P(fmt,...)
#define log_warn_1_P(fmt)
#endif

#define log(fmt,...) printf (fmt,__VA_ARGS__);
#define log_1(fmt) printf (fmt);
#define log_P(fmt,...) printf_P (PSTR(fmt), __VA_ARGS__);
#define log_1_P(fmt) printf_P (PSTR(fmt));


#endif /* __LOG_LIB_H_ */
