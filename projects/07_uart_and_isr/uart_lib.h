/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * uart_lib.h: UART handling module
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#ifndef __UART_LIB_H_
#define __UART_LIB_H_

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <stdio.h>

void uart_init (void);
int uart_putchar (char c, FILE* stream);
int uart_getchar (FILE* stream);

extern FILE uart_output;
extern FILE uart_input;

#endif /* __UART_LIB_H_ */
