/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * uart_and_isr.c: Quick'n'Dirty(tm) testing for ISR for measurement
 * series.
 *
 * - :TODO: Needs cleanup, the CLI seems mostly turned-off.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

/*

  !!! Assure you're using 3.3V max !!!

  Interconnection

  ATmega328P-PU   |        23K256

  PB5 (SCK) <--------->  PIN6 (SCK)
  PB4 (MISO) <-------->  PIN2 (SO)
  PB3 (MOSI) <-------->  PIN5 (SI)
  PB2 (SS) <---------->  PIN1 (CS)
                         PIN4 (VSS) <--> GND
						 PIN8 (VCC) <--> VCC (3.3V)
						 PIN7 (HOLD) <-> VCC (3.3V)

  For more information about this project, see:

    NOTES.uart_and_isr.org
*/

#ifndef EMU
#include <avr/interrupt.h>

int got_interrupted = 0;
// OLD SIG_OVERFLOW1
ISR(TIMER1_OVF_vect)
{
	got_interrupted = 1;
	reti();
}
#endif

#include <stdlib.h>
#include <stdio.h>


#include <string.h> // strcmp()
#include <inttypes.h>

#ifndef EMU
 #include <avr/io.h>
 #include "uart_lib.h"
#endif
#include "23k256_lib.h"

#include "log_lib.h"

#include <ctype.h> /* for iscntrl() */
void read_line (char* input_buf, int max_len, FILE* stream);

enum _cmd_t {
	cmd_read_byte = 0,
	cmd_write_byte = 1,
	cmd_get_status = 2,
	cmd_put_status = 3,
	cmd_init = 4,
	cmd_read_page = 5,
	cmd_write_page = 6,
	cmd_read_seq = 7,
	cmd_write_seq = 8,
	cmd_help = 9,
	cmd_error = 10
};
typedef enum _cmd_t cmd_t;

//#define ONLINE_HELP
#ifdef ONLINE_HELP

#if defined __FLASH
static __flash const char cmd_init_usage[] =
	"init - (re-)initialize SPI\n"
	"usage: init\n";
static __flash const char cmd_get_status_usage[] =
	"get stat - read mode from status register\n"
	"usage: get stat\n";
static __flash const char cmd_put_status_usage[] =
	"put stat - write mode to status register\n"
	"usage: put stat <mode>\n"
	"  byte mode = 0, page mode = 128, cont. mode = 64\n";
static __flash const char cmd_mget_usage[] =
	"get byte - read from memory address (byte mode)\n"
	"usage: get byte <address>\n";
static __flash const char cmd_write_byte_usage[] =
	"put byte - write to memory address (byte mode)\n"
	"usage: put byte <address> <value>\n";
static __flash const char cmd_read_page_usage[] =
	"get page - read page from memory address (page mode)\n"
	"usage: get page <page> [start_at_addr]\n";
static __flash const char cmd_pput_usage[] =
	"put page - write pattern to page at memory address (page mode)\n"
	"usage: put page <page> [start_at_addr]\n";

static __flash const char* usage_vec[] = {
	cmd_read_byte_usage,
	cmd_write_byte_usage,
	cmd_get_status_usage,
	cmd_put_status_usage,
	cmd_init_usage,
	cmd_read_page_usage,
	cmd_write_page_usage
};

#elif defined EMU

static const char cmd_init_usage[] =
	"init - (re-)initialize SPI\n"
	"usage: init\n";
static const char cmd_get_status_usage[] =
	"get stat - read mode from status register\n"
	"usage: get stat\n";
static const char cmd_put_status_usage[] =
	"put stat - write mode to status register\n"
	"usage: put stat <mode>\n"
	"  byte mode = 0, page mode = 128, cont. mode = 64\n";
static const char cmd_read_byte_usage[] =
	"get byte - read from memory address (byte mode)\n"
	"usage: get byte <address>\n";
static const char cmd_write_byte_usage[] =
	"put byte - write to memory address (byte mode)\n"
	"usage: put byte <address> <value>\n";
static const char cmd_read_page_usage[] =
	"get page - read page from memory address (page mode)\n"
	"usage: get page <page> [start_at_addr]\n";
static const char cmd_write_page_usage[] =
	"put page - write pattern to page at memory address (page mode)\n"
	"usage: put page <page> [start_at_addr]\n";

static const char* usage_vec[] = {
	cmd_read_byte_usage,
	cmd_write_byte_usage,
	cmd_get_status_usage,
	cmd_put_status_usage,
	cmd_init_usage,
	cmd_read_page_usage,
	cmd_write_page_usage
};
#else

 #error "No flash namespace available"

#endif /* FLASH , EMU */
#endif /* ONLINE_HELP */


#ifdef ONLINE_HELP
int
print_usage (cmd_t cmd)
{
	/* cmd_err signals to print all command usages */
	if (cmd == cmd_error) {
		log ("%s\n%s\n%s\n%s\n%s\n%s\n%s"
			 ,cmd_init_usage, cmd_get_status_usage, cmd_put_status_usage
			 ,cmd_read_byte_usage, cmd_write_byte_usage, cmd_read_page_usage
			 ,cmd_write_page_usage);
	}
	else {
		log ("%s", (usage_vec[cmd])); // This works!  At least in EMU mode ;)
	}
	return 0;
}
#endif /* ONLINE_HELP */

void
read_line (char* input_buf, int max_len, FILE* stream)
{
	int c = EOF;
	char* p = input_buf;
	int cur_len = max_len;
	
	if (cur_len < 2)
		return;
	
	for (;;) {
		c = getc (stream);
		
		switch (c) {
			
		case EOF:
			*p = '\0';
			return;
			
		// TODO:
		//  - 0x04 (end of transmission -- (execute exit)
		//  - Use '\f' analog to '\n'
		case '\n':
		case '\r':
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
			
		case '\f': // form feed
			putchar ('\f');
			break;
			
		case '\b': // back space
			if ( (p - input_buf) != 0 ) {
				p--;
				cur_len--;
				putchar ('\b');
			}				
			break;
			
		default:
			if (iscntrl (c)) {
				// DEBUG output
				printf ("%x", c);
			}
			else {// echo character and write into buffer
				*p = (char) c;
				p++;
				cur_len--;
				putchar (c);
			}
		}
		
		if (cur_len <= 1) {
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
		}
	}	
	return;
}
	
int
eat_byte_val (char** input, uint8_t* byte_val)
{
	int n = 0;
	/* int eat = sscanf (*input, "%u"SCNu8, *byte_val); */
	int eat = sscanf (*input, "%hhd%n", byte_val, &n);
	
	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}

int
eat_mem_address (char** input, sram_mem_address_t* addr)
{
	int n = 0;
	/* int eat = sscanf (*input, "u%"SCNu16"%n", addr, &n); */
	int eat = sscanf (*input, "%hd%n", addr, &n);

	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}


int
eat_cmd (char** input)
{
	char _cmd_1[4 + 1] = ""; // cmd first part get, put, init, help (length 4 + 1)
	char _cmd_2[4 + 1] = ""; // cmd second part (max length 4 + 1)
	enum _cmd_class { NONE, GET, PUT };
	typedef enum _cmd_class cmd_class;
	cmd_class class = NONE;

	int n = 0;
	sscanf (*input, "%s%n", _cmd_1, &n);

	if (!strncmp (_cmd_1, "init", 4)) {
		*input += n;
		return cmd_init;
	}
	else if (!strncmp (_cmd_1, "get", 3)) {
		class = GET;
		*input += n;
	}
	else if (!strncmp (_cmd_1, "put", 3)) {
		class = PUT;
		*input += n;
	}
#ifdef ONLINE_HELP
	else if (!strncmp (_cmd_1, "help", 4)) {
		*input += n;
		return cmd_help;
	}
#endif /* ONLINE_HELP */

	else {
		// error
		return cmd_error;
	}

	sscanf (*input, "%s%n", _cmd_2, &n);
	if (!strncmp (_cmd_2, "byte", 4)) {
		*input +=n;
		return (class == GET) ? cmd_read_byte : cmd_write_byte;
	}
	else if (!strncmp (_cmd_2, "page", 4)) {
		*input +=n;
		return (class == GET) ? cmd_read_page : cmd_write_page;
	}
	else if (!strncmp (_cmd_2, "stat", 4)) {
		*input += n;
		return (class == GET) ? cmd_get_status : cmd_put_status;
	}
	else if (!strncmp (_cmd_2, "seq", 3)) {
		*input += n;
		return (class == GET) ? cmd_read_seq : cmd_write_seq;
	}
	else {
		return cmd_error;
	}
}

int
parse_cmd_line (char** input_buf)
{
	char* p_input_buf = *input_buf;
	cmd_t cmd = cmd_error;
	sram_mem_address_t    mem_addr = 0;
	// TODO
	/* sram_mem_address_t* p_mem_addr = &mem_addr; */
	uint8_t    byte_val = 0;
	// TODO
	/* uint8_t* p_byte_val = &byte_val; */
	uint8_t page[page_size];
	uint8_t i = 0;

	log_debug ("before *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);
	cmd = eat_cmd (&p_input_buf);
	log_debug ("after *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);

	if (cmd == cmd_error) {
		log ("Error: Wrong cmd:\n  %s\n", p_input_buf);
		return -1;
	}

	switch (cmd) {
#ifdef ONLINE_HELP
	case cmd_help:
		{
			cmd_t help_for_cmd = eat_cmd (&p_input_buf);
			print_usage (help_for_cmd);
		}
		break;
#endif /* ONLINE_HELP */
		
	case cmd_init:
		log_info_1 ("Initialization\n");
		sram_init_spi();
		break;

	case cmd_read_page:
		{
			sram_mem_address_t addr_in_page;
			if (!eat_mem_address (&p_input_buf, &mem_addr)) {
				log_1 ("Error: memory address expected!\n");
				return -1;
			}
			if (eat_mem_address (&p_input_buf, &addr_in_page)) {
				if (addr_in_page > 32) {
					log_1 ("Error: address in page is too big!\n");
					return -1;
				}
				log_info ("read_page: %d starting at: %d\n", mem_addr, addr_in_page);
				sram_read_page (page, mem_addr, addr_in_page);
				if ((addr_in_page % 8) != 0) {
					log_info_1 ("\t");
					for (int j = 0; j < (addr_in_page % 8); j++) {
						printf ("     ");
					}
				}
				for (i = addr_in_page; i < 32; i++) {
					if ( ((i+1) % 8) == 1 ) {
						log_info ("\t0x%02x ", page[i]);
					}
					else {
						printf ("0x%02x%s", page[i], ( (!((i+1) % 8)) ? "\n" : " "));
					}
				}
			}
			else {
				log_info ("read_page: %d\n", mem_addr);
				sram_read_page (page, mem_addr, 0);
				for (i = 0; i < 32;) {
					log_info ("\t0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\n"
							  , page[i],     page[i + 1], page[i + 2], page[i + 3]
							  , page[i + 4], page[i + 5], page[i + 6], page[i + 7]
							  );
					i += 8;
				}
			}
		}
		break;
		
	case cmd_write_page:
		{
			sram_mem_address_t addr_in_page;
			
			if (!eat_mem_address (&p_input_buf, &mem_addr)) {
				log_1 ("Error: memory address expected!\n");
				return -1;
			}
			if ((mem_addr < 0) || (mem_addr > 1023)) {
				log ("Error: invalid page number given %d\n", mem_addr);
				return -1;
			}

			if (eat_mem_address (&p_input_buf, &addr_in_page)) {
				if (addr_in_page > 32) {
					log_1 ("Error: address in page is too big!\n");
					return -1;
				}
				log_info ("Page write, writing pattern to page: %d, starting at %d\n"
						  , mem_addr, addr_in_page);
				{
					int val = (mem_addr % 32) + addr_in_page;
					for (int i = addr_in_page; i < 32; i++) {
						page[i] = val++;
					}
				}
				sram_write_page (mem_addr, page, addr_in_page);
			}
			else {
				log_info ("Page write, writing pattern to full page: %d\n", mem_addr);
				{
					int val = (mem_addr % 32);
					for (int i = 0; i < 32; i++) {
						page[i] = val++;
					
					}
				}
				sram_write_page (mem_addr, page, 0);
			}
		}
		break;
		
	case cmd_read_byte:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			printf ("Error: memory address expected!\n");
			return -1;
		}
		// TODO
		/* log_debug ("Success: Following command read: \n" \ */
		/* 		"  cmd: %s,  @: %d\n", cmds[cmd], mem_addr); */

		{
			uint8_t read_byte = sram_read_byte (mem_addr);
			log_info ("read byte @%d: 0x%02x\n", mem_addr, read_byte);
		}
		break;
		
	case cmd_write_byte:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			log_1 ("Error: memory address expected!\n");
			return -1;
		}
		if (!eat_byte_val (&p_input_buf, &byte_val)) {
			log_1 ("Error: byte val expected!\n");
			return -1;
		}

		// TODO
		/* log_debug ("Success: Following command read: \n" */
		/* 		"  cmd: %s,  @: %d, val: %d\n", cmds[cmd], mem_addr, byte_val); */

		sram_write_byte (mem_addr, byte_val);
		break;

	case cmd_get_status:
		// TODO
		/* log_debug ("Success: Following command read: \n" */
		/* 		"  cmd: %s\n", cmds[cmd]); */

		{
			sram_op_mode mode = sram_read_status_register ();
			log_info ("status register returns: 0x%02x\n", mode);
		}
		
		break;
		
	case cmd_put_status:
		if (!eat_byte_val (&p_input_buf, &byte_val)) {
			log_1 ("Error: byte val expected!\n");
			return -1;
		}

		// TODO
		/* log_debug ("Success: Following command read: \n" */
		/* 		"  cmd: %s,  val: %d\n", cmds[cmd], byte_val); */

		{
			sram_op_mode mode = byte_val;
			log_debug ("mset() setting mode: 0x%02x\n", mode);
			sram_write_status_register (byte_val);
		}
		break;

	}
	return cmd;
}

/* -------------------------------------------------------- */

int
main (void)
{
	unsigned char was_set = 0;

#ifndef EMU
	// Set port C's Pin 
	DDRC  = 0xFE; // set Port C, all pins as output except pin0 as input
	PORTC = 0x01; // set all output pins to 0, and set pin0 (input) to pull-up
#endif /* EMU */

#define LINE_LEN 80

	char input_buf[LINE_LEN];
	char* p_buf = input_buf;

#ifndef EMU	
	uart_init();
#endif
	sram_init_spi();

#ifndef EMU	
	// Turn on TIMER1, without prescaler, this creates the fastet
	// triggering of timer1
	TCCR1B |= _BV(CS10);
	TIMSK1 |= _BV(TOIE1);
	sei();	// Turn interrupts on.
#endif

	for (;;) {
		fputs ("# ", stdout);

#ifndef EMU		
		if (got_interrupted) {
			fputs ("!! Got interrupted\n", stdout);
			got_interrupted = 0;
			continue;
		}
#endif
		
 		read_line (input_buf, sizeof (input_buf) - 1, stdin);
		if (strnlen (input_buf, LINE_LEN) == 0)
			continue;
		parse_cmd_line (&p_buf);

#ifndef EMU
		if (!was_set) {
			PORTC |= _BV(1);
			PORTC &= ~(_BV(2));
			was_set = 1;
		}
		else {
			PORTC |= _BV(2);
			PORTC &= ~(_BV(1));
			was_set = 0;
		}
#endif /* EMU */
	}
	return 0;
}
