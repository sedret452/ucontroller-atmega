/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * 23k256_lib.h: SPI access module for SRAM chip (Microchip 23K256)
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#ifndef __23K256_LIB_H_
#define __23K256_LIB_H_

#if (MCU != atmega328p)
 #error "23k256_lib is only usable for ATmega328p!  Be sure to set properly MCU variable."
#endif

#include <inttypes.h>

enum _sram_instruction { READ = 0x03, WRITE = 0x02, RDSR = 0x05, WRSR = 0x01 };
extern const uint8_t  page_size;
extern const uint16_t max_page;

enum _sram_op_mode {
	BYTE_MODE = 0x00,
	PAGE_MODE = (1<<7), // 128 dec
	CONT_MODE = (1<<6), // 64  dec
	RESERVED_MODE = (1<<6) | (1<<7)// 0x03
};
typedef enum _sram_op_mode sram_op_mode;
typedef uint16_t sram_mem_address_t;

void sram_init_spi ();
sram_op_mode sram_read_status_register ();
int sram_write_status_register (sram_op_mode mode);
uint8_t sram_read_byte (sram_mem_address_t address);
int sram_write_byte (sram_mem_address_t address, uint8_t val);
int sram_read_page (uint8_t* val_array, uint8_t page, uint8_t addr_in_page);
int sram_write_page (uint8_t page, uint8_t* val_array, uint8_t addr_in_page);
int sram_read_sequential (uint8_t* val_vec, uint16_t len, uint8_t page
						  , uint8_t addr_in_page);
int sram_write_sequential (uint8_t page, uint8_t addr_in_page, uint8_t* val_vec
						   , uint16_t len);

#endif /* __23K256_LIB_H_ */
