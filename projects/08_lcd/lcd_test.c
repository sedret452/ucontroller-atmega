/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * lcd_test.c: Clock example with 2-row text LCD and UART-based CLI.
 *
 * Based on the HD44780 LCD driver by Joerg
 *  - lcd.{h,c}, hd44780.{h,c}
 *
 * This is *not* an RTC but prep-work for timestamping inside sensor
 * node.
 *
 * A rather complex CLI is provided, see the definitions of:
 *   cmd_..._usage[]
 * to get a grasp.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // strcmp()
#include <inttypes.h>

#include "log_lib.h"

enum _cmd_t {
	cmd_error = 0
	, cmd_help = 1
	, cmd_lcd_init = 2
	, cmd_lcd_write_line = 3
	, cmd_lcd_clear_line = 4
	, cmd_lcd_write_special = 5
	, cmd_lcd_rotate = 6
	, cmd_time_get = 7
	, cmd_date_get = 8
	, cmd_time_put = 9
	, cmd_date_put = 10
	, cmd_lcd_write_time = 11
	, cmd_lcd_write_date = 12
	, cmd_isr_set = 13
	, cmd_isr_unset = 14
};
typedef enum _cmd_t cmd_t;

#define ONLINE_HELP
#ifdef ONLINE_HELP


#if defined __FLASH

#include <avr/pgmspace.h>

static __flash const char cmd_lcd_init_usage [] =
	"lcdi - Init LCD\n"
	"\tusage: lcdi\n";

static __flash const char cmd_lcd_write_line_usage [] =
	"lcdw - Write a line to LCD\n"
	"\tusage: lcdw \"<LINE>\"\n";

static __flash const char cmd_lcd_clear_line_usage [] =
	"lcdc - Clear a line of LCD\n"
	"\tusage: lcdc\n";

static __flash const char cmd_lcd_write_time_usage [] =
	"lcdt - Write current time to LCD\n"
	"\tusage: lcdt\n";

static __flash const char cmd_lcd_write_date_usage [] =
	"lcdd - Write current date to LCD\n"
	"\tusage: lcdd\n";

static __flash const char cmd_lcd_write_special_usage [] =
	"lcds - Write special to LCD\n"
	"\tusage: lcds\n";

static __flash const char cmd_lcd_rotate_usage [] =
	"lcdr - Rotate LCD row\n"
	"\tusage: lcdr <row> <num> <direction>\n";

static __flash const char cmd_time_get_usage [] =
	"tget - Read current time\n"
	"\tusage: tget\n";

static __flash const char cmd_date_get_usage [] =
	"dget - Read current date\n"
	"\tusage: dget\n";

static __flash const char cmd_time_put_usage [] =
	"tput - Read in time\n"
	"\tusage: tget \"00:00:00\"\n";

static __flash const char cmd_date_put_usage [] =
	"dput - Read in date\n"
	"\tusage: dget \"00/00/0000\"\n";

static __flash const char cmd_isr_set_usage [] =
	"isrs - ISR set\n"
	"\tusage: isrs\n";

static __flash const char cmd_isr_unset_usage [] =
	"isru - ISR unset\n"
	"\tusage: isru\n";


static __flash const char* usage_vec[] = {
	cmd_lcd_init_usage
	, cmd_lcd_write_line_usage
	, cmd_lcd_clear_line_usage
	, cmd_lcd_write_special_usage
	, cmd_lcd_rotate_usage
	, cmd_time_get_usage
	, cmd_date_get_usage
	, cmd_time_put_usage
	, cmd_date_put_usage
	, cmd_lcd_write_time_usage
	, cmd_lcd_write_date_usage
	, cmd_isr_set_usage
	, cmd_isr_unset_usage
};

#elif defined EMU

 #error "No emulation supported for m93c86 library"

};
#else

 #error "No flash namespace available"

#endif /* FLASH , EMU */
#endif /* ONLINE_HELP */

#ifdef ONLINE_HELP

int
print_usage (cmd_t cmd)
{
	if (cmd == cmd_error) {
#ifdef __FLASH

		// I know, %S has usually a different meaning, so lets turn
		// off type warnings here
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
		
		printf ("%S%S%S%S%S%S%S%S%S%S%S%S%S"
				, cmd_lcd_init_usage
				, cmd_lcd_write_line_usage
				, cmd_lcd_clear_line_usage
				, cmd_lcd_write_special_usage
				, cmd_lcd_rotate_usage
				, cmd_time_get_usage
				, cmd_date_get_usage
				, cmd_time_put_usage
				, cmd_date_put_usage
				, cmd_lcd_write_time_usage
				, cmd_lcd_write_date_usage
				, cmd_isr_set_usage
				, cmd_isr_unset_usage
				);
		
		
#pragma GCC diagnostic pop		
		
#else
		log ("%s\n%s\n%s\n%s\n%s\n%s\n%s"
			 ,cmd_lcd_write_line_usage
			 ,cmd_lcd_clear_line_usage
			 );
#endif /* __FLASH */		
	}
	else {
#ifdef __FLASH
		printf ("%S\n", usage_vec[cmd]);
#else
		log ("%s", (usage_vec[cmd])); // This works!  At least in EMU mode ;)
#endif
	}
	return 0;
}
#endif /* ONLINE_HELP */





#ifndef EMU

#include <avr/io.h>
#include "uart_lib.h"
#include "lcd.h"
#include "time_lib.h"

#include <avr/sleep.h>
#include <avr/interrupt.h>




int cur_time_update = 0;

// TIMER2_OVF_vect is referenced as "SIG_OVERFLOW2" in older
// documentation!
ISR(TIMER2_OVF_vect)
{
	time_increment();

	//if ((cur_time.secs % 10) == 0) {
	char time_s[] = "00:00:00";
	time_fill_time_string (time_s);
	char date_s[] = "00/00/0000";
	time_fill_date_string (date_s);
	lcd_write_line (date_s, 11, LCD_R0 | LCD_C | LCD_SKIP_CLR);
	lcd_write_line (time_s,  9, LCD_R1 | LCD_C | LCD_SKIP_CLR);
		//}
}

// Used when Timer 2 Compare Mode (CTC) used
ISR(TIMER2_COMPA_vect)
{
	time_increment_by(6);

	//if ((cur_time.secs % 10) == 0) {
	char time_s[] = "00:00:00";
	time_fill_time_string (time_s);
	char date_s[] = "00/00/0000";
	time_fill_date_string (date_s);
	lcd_write_line (date_s, 11, LCD_R0 | LCD_C | LCD_SKIP_CLR);
	lcd_write_line (time_s,  9, LCD_R1 | LCD_C | LCD_SKIP_CLR);
		//}
}

/* Provide an interrupt each 6s, using Timer2 in Compare Mode (CTC
 * mode).
 */
void
activate_timer2_6s()
{
#ifndef EMU	
	// Turn on TIMER2, using a prescaler of 1024. It is expected that
	// a 32765 Hz clock is used, asynchrounsly to the system clock

	// Select external Crystal Oscillator as source (Chapter 18.11.8)
	ASSR |= _BV(AS2);

	// Select 1024 Divider
	TCCR2B |= _BV(CS22) | _BV(CS21) | _BV(CS20); // Write 0b111

	// Reset Timer Counter
	TCNT2 = 0;

	// Use CTC mode
	TCCR2B |= _BV(WGM22);
	TCCR2A |= _BV(WGM21);

	// I'm trying out Compare Mode here, but the intention is
	// that the Interrupt won't change the PIN output behavior
	TIMSK2 |= _BV(OCIE2A);
	
	// Using CLC mode the OCR2A register defines the top/barrier value
	// for timer counter, when it is cleared to zero
	// 192 * 31.25ms = 6000ms = 6s
	OCR2A = 192 - 1;
	/* OCR2A = 96 - 1; // Use 3s */

	sei();	// Turn interrupts on.
#endif
}

/* Provide an interrupt each 1s, using Timer2 in Normal Mode (counter
 * mode with overflow).
 */
void
activate_timer2()
{
#ifndef EMU
	// Turn on TIMER2, using a prescaler of 128
	//
	// It is expected that a 32765 Hz clock is used.
	ASSR |= _BV(AS2);
	//
	//   Clock Divider = 128 and 256 counter values
	//   => 1s
	//

	// 128 Divider = Bits "101" -> CS22 <-1,  CS21 <-0,  CS20 <-1
	TCCR2B |= _BV(CS22) | _BV(CS20);
	TIMSK2 |= _BV(TOIE2);
	
	sei();	// Turn interrupts on.
#endif
}

void
deactivate_timer2()
{
#ifndef EMU
	TCCR2B = 0; // Turn off timer2
#endif
}
#endif


#include <ctype.h> /* for iscntrl() */
void read_line (char* input_buf, int max_len, FILE* stream);





void
read_line (char* input_buf, int cur_len, FILE* stream)
{
	int c = EOF;
	char* p = input_buf;

	*p = '\0'; // safe initialization
	
	if (cur_len < 2)
		return;
	
	for (;;) {
		c = getc (stream);
		
		switch (c) {
			
		case EOF:
			*p = '\0';
			return;
			
		// TODO:
		//  - 0x04 (end of transmission -- (execute exit)
		//  - Use '\f' analog to '\n'
		case '\n':
		case '\r':
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
			
		case '\f': // form feed
			putchar ('\f');
			putchar ('#');
			putchar (' ');
			break;
			
		case '\b': // back space
			if ( (p - input_buf) != 0 ) {
				p--;
				cur_len++;
				putchar ('\b');
				putchar (' ');
				putchar ('\b');
			}
			break;
			
		default:
			if (iscntrl (c)) {
				// DEBUG output
				log_warn_P ("Unknown control sequence: %x", c);
			}
			else {// echo character and write into buffer
				*p = (char) c;
				p++;
				cur_len--;
				putchar (c);
			}
		}
		
		if (cur_len <= 1) {
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
		}
	}	
	return;
}

int
eat_cmd (char** input)
{
	char _cmd_1[4 + 1] = ""; // cmd first part lcdc, lcdw, help (length 4 + 1)
	/* char _cmd_2[4 + 1] = ""; // cmd second part (max length 4 + 1) */
	int n = 0;
	
	sscanf_P (*input, PSTR("%s%n"), _cmd_1, &n);
	
	if (!strncmp_P (_cmd_1, PSTR("lcdi"), 4)) {
		*input += n;
		return cmd_lcd_init;
	}
	else if (!strncmp_P (_cmd_1, PSTR("lcdc"), 4)) {
		*input += n;
		return cmd_lcd_clear_line;
	}
	else if (!strncmp_P (_cmd_1, PSTR("lcdw"), 4)) {
		*input += n;
		return cmd_lcd_write_line;
	}
	else if (!strncmp_P (_cmd_1, PSTR("lcds"), 4)) {
		*input += n;
		return cmd_lcd_write_special;
	}
	else if (!strncmp_P (_cmd_1, PSTR("lcdr"), 4)) {
		*input += n;
		return cmd_lcd_rotate;
	}
	else if (!strncmp_P (_cmd_1, PSTR("tget"), 4)) {
		*input += n;
		return cmd_time_get;
	}
	else if (!strncmp_P (_cmd_1, PSTR("tput"), 4)) {
		*input += n;
		return cmd_time_put;
	}
	else if (!strncmp_P (_cmd_1, PSTR("dget"), 4)) {
		*input += n;
		return cmd_date_get;
	}
	else if (!strncmp_P (_cmd_1, PSTR("dput"), 4)) {
		*input += n;
		return cmd_date_put;
	}
	else if (!strncmp_P (_cmd_1, PSTR("lcdt"), 4)) {
		*input += n;
		return cmd_lcd_write_time;
	}
	else if (!strncmp_P (_cmd_1, PSTR("lcdd"), 4)) {
		*input += n;
		return cmd_lcd_write_date;
	}
	else if (!strncmp_P (_cmd_1, PSTR("isrs"), 4)) {
		*input += n;
		return cmd_isr_set;
	}
	else if (!strncmp_P (_cmd_1, PSTR("isru"), 4)) {
		*input += n;
		return cmd_isr_unset;
	}
#ifdef ONLINE_HELP
	else if (!strncmp_P (_cmd_1, PSTR("help"), 4)) {
		*input += n;
		return cmd_help;
	}
#endif /* ONLINE_HELP */

	else {
		// error
		return cmd_error;
	}
}


int
parse_cmd_line (char** input_buf)
{
	char* p_input_buf = *input_buf;
	cmd_t cmd = cmd_error;

	log_debug_P ("before *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);
	cmd = eat_cmd (&p_input_buf);
	log_debug_P ("after *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);

	if (cmd == cmd_error) {
		log_P ("Error: Wrong cmd:\n  %s\n", p_input_buf);
		return -1;
	}

	switch (cmd) {
#ifdef ONLINE_HELP
	case cmd_help:
		{
			cmd_t help_for_cmd = eat_cmd (&p_input_buf);
			print_usage (help_for_cmd);
		}
		break;
#endif /* ONLINE_HELP */

	case cmd_lcd_init:
		log_info_1_P ("LCD init\n");
		lcd_init();
		break;
		
	case cmd_lcd_clear_line:
		log_info_1_P ("LCD clear line\n");
		lcd_putchar ('\n', NULL);
		break;

	case cmd_lcd_write_special:
		/* Based on ROM Code: A00 character codes*/
		log_info_1_P ("LCD write special\n");
		{
			char _lcd_line[17+1] = "";
			_lcd_line[0] = 0xe1; // ä
			_lcd_line[1] = 0xf5; // ü
			_lcd_line[2] = 0xef; // ö
			_lcd_line[3] = 0xe2; // ß

			_lcd_line[4] = 0xff; // full blank
			_lcd_line[5] = '\n';
			_lcd_line[6] = '\0';

			for (int i = 0
					 ; i < 17 && _lcd_line[i] != '\n' && _lcd_line[i] != '\0'
					 ; ++i)
				{
					log_info_P ("\t \[%d]: %c, 0x%X, n1 0x%X, n2 0x%X\n"
								, i
								, _lcd_line[i]
								, _lcd_line[i]
								, _lcd_line[i] >> 4
								, _lcd_line[i] & 0xf
								);
					lcd_putchar (_lcd_line[i], NULL);					
				}
		}
		lcd_putchar ('\n', NULL);
		break;
		
		
	case cmd_lcd_write_line:
		log_info_1_P ("LCD write line\n");
		log_info_P ("\tFull Line: %s\n", *input_buf);
		{
			char _lcd_line[17 + 1] = "";
			int n = 0;
			sscanf_P (p_input_buf, PSTR(" %16c"), _lcd_line);
			/* sscanf_P (p_input_buf, PSTR("%s%n"), _lcd_line, &n); */

			log_info_P ("\tLCD >%s<\n", _lcd_line);

			lcd_write_line (_lcd_line, 17 + 1, 2);

			/* for (int i = 0 */
			/* 		 ; i < 17 && _lcd_line[i] != '\n' && _lcd_line[i] != '\0' */
			/* 		 ; ++i) */
			/* 	{ */
			/* 		log_info_P ("\t \[%d]: %c, 0x%X, n1 0x%X, n2 0x%X\n" */
			/* 					, i */
			/* 					, _lcd_line[i] */
			/* 					, _lcd_line[i] */
			/* 					, _lcd_line[i] >> 4 */
			/* 					, _lcd_line[i] & 0xf */
			/* 					); */
								
			/* 		lcd_putchar (_lcd_line[i], NULL);					 */
			/* 	} */
			
			/* for (int i = 0 */
			/* 		 ; i < 17 && _lcd_line[i] != '\n' && _lcd_line[i] != '\0' */
			/* 		 ; ++i) */
			/* 	{ */
			/* 		lcd_putchar (_lcd_line[i], NULL);					 */
			/* 	} */

			/* for (int i = 0 */
			/* 		 ; i < 17 && _lcd_line[i] != '\n' && _lcd_line[i] != '\0' */
			/* 		 ; ++i) */
			/* 	{ */
			/* 		lcd_putchar (_lcd_line[i], NULL);					 */
			/* 	} */
			
		}
		/* lcd_putchar ('\n', NULL); */

		break;

	case cmd_lcd_rotate:
		log_info_1_P ("LCD rotate row\n");
		{
			char row = 1;
			char num = 0;
			char direction = 0;
			sscanf_P (p_input_buf, PSTR("%d %d %d"), &row, &num, &direction);

			lcd_shift_display_row (row, num, direction);
		}
		break;
		
	case cmd_time_get:
		{
			char s[9] = "";
			time_fill_time_string (s);
			log_P ("Current time: %s\n", s);
		}
		break;

	case cmd_date_get:
		{
			char s[11] = "";
			time_fill_date_string (s);
			log_P ("Current date: %s\n", s);
		}
		break;

	case cmd_time_put:
		{
			char s[] = "23:45:00";
			time_read_time_string (p_input_buf);

			time_fill_time_string (s);
			log_P ("Current time: %s\n", s);
		}
		break;

	case cmd_date_put:
		{
			char s[] = "15/12/2017";
			time_read_date_string (p_input_buf);

			time_fill_date_string (s);
			log_P ("Current date: %s\n", s);
		}
		break;

	case cmd_lcd_write_time:
		{
			char s[] = "00:00:00";
			time_fill_time_string (s);
			lcd_write_line (s, 9, LCD_R1 | LCD_C | LCD_SKIP_CLR);
		}
		break;

	case cmd_lcd_write_date:
		{
			char s[] = "00/00/0000";
			time_fill_date_string (s);
			lcd_write_line (s, 11, LCD_R0 | LCD_C | LCD_SKIP_CLR);
		}
		break;

	case cmd_isr_set:
		activate_timer2();
		//activate_timer2_6s();
		break;
		
	case cmd_isr_unset:
		deactivate_timer2();
		break;
		

	}
	return cmd;
}



/* -------------------------------------------------------- */

int
main (void)
{
#define LINE_LEN 40

	char input_buf[LINE_LEN];
	char* p_buf = input_buf;

#ifndef EMU
	// Set port C's Pin 
	//DDRC  = 0xFE; // set Port C, all pins as output except pin0 as input
	//PORTC = 0x01; // set all output pins to 0, and set pin0 (input) to pull-up
	lcd_init();
	
	uart_init();
#endif
	/* configure_timer1(); */
	
	for (;;) {
		putchar ('#');
		putchar (' ');

 		read_line (input_buf, sizeof (input_buf) - 1, stdin);
		if (strnlen (input_buf, LINE_LEN) == 0)
			continue;
		
		parse_cmd_line (&p_buf);

		
	}
	return 0;
}
