/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * leapyear_test.c: Simple testing facility for time_lib module.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include <stdio.h>
#include <stdlib.h> // srand48(), lrand48()
#include <time.h>
#include <inttypes.h>

#include "time_lib.h"

#define YEARSIZE(year)  (LEAPYEAR(year) ? 366 : 365)
#define LEAPYEAR(year)  (!((year) % 4) && (((year) % 100) || !((year) % 400)))

time_t
get_random_time ()
{
	// Returns a value between:
	// a) 0 --> 1970-01-01 00:00:00
	// b) (2^31 - 1) --> Dunno
	return lrand48();
}


int
test_leapyear_macro()
{
	
	srand48(0xDEAD);

	for (int i = 0; i < 100; ++i) {
		// 1. Randomly select a date
		time_t t_orig = get_random_time();

		// 2. Translate unix time to "normal" time
		struct tm tm_orig = { .tm_isdst=0 };

		if (!gmtime_r (&t_orig, &tm_orig)) {
			printf ("Error: Translation of epoche time failed (tm_orig)\n");
			return -1;
		}

		uint16_t year = tm_orig.tm_year;
		int result_func  = is_leap_year(&year);
		int result_macro = LEAPYEAR(year);

		if (result_func != result_macro) {
			printf ("Error: Testing for leapyear failed!\n");
			return -1;
		}
	}

	return 0;
}

int
main (int argc, char** argv)
{
	return 0;
}
