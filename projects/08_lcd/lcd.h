/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <joerg@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.        Joerg Wunsch
 * ----------------------------------------------------------------------------
 *
 * Stdio demo, upper layer of LCD driver.
 *
 * $Id$
 */

/*
 * Initialize LCD controller.  Performs a software reset.
 */
void	lcd_init(void);

/*
 * Send one character to the LCD.
 */
int	lcd_putchar(char c, FILE *stream);

#define LCD_ROW_MASK 0b00000011
#define LCD_R0  	 0b00000000  // Row 0 (from top)
#define LCD_R1  	 0b00000001  // Row 1
#define LCD_C   	 0b00000100  // Centered
#define LCD_SKIP_CLR 0b00001000  // Skip clearing display

int
lcd_write_line (char* line, char line_len, uint8_t props);

int
lcd_shift_display_row (char display_row, char len, char direction);
