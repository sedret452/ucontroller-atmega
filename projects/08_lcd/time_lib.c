/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * time_lib.c: Time module, typical time handling functions.
 *
 * Copyright	(C) 2013-2022 sedret452
 *
 * Note: IN avrlibc 2.0 time.h is available (see:
 * http://www.nongnu.org/avr-libc/user-manual/group__avr__time.html)
 *
 * In older versions this wasn't available -- or didn't fit the needs
 * of the projects (?) -- hence required time functions (and some
 * more) were recreated from scratch; and adjusted to the individual
 * requirements of the project.
 */

#include <inttypes.h>
#include <stdint.h> // UINT32_C, UINT16_C, UINT8_C
#include <stdio.h>

#if !defined(EMU) && defined(__FLASH)
#include <avr/pgmspace.h>
#endif

#include "time_lib.h"

#include "log_lib.h"


uint8_t
is_leap_year (const uint16_t year)
{
	// return (!((year) % 4) && (((year) % 100) || !((year) % 400)));
    return (((year % 4) == 0) && (((year % 100) != 0) || ((year % 400) == 0)));
}

uint16_t
get_yearsize (const uint16_t year)
{
	if (is_leap_year (year)) {
		return 366;
	}
	else {
		return 365;
	}
}

uint16_t
get_current_day_of_year ()
{
	const uint16_t days[2][12] =
		{
			{ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 },
			{ 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 },
		};
	
	/* uint8_t days[] = {31, (is_leap_year(cur_time.years) ? 29 : 28), 31, 30, 31 */
	/* 				  , 30, 31, 31, 30, 31, 30, 31}; */

	// Initialize by using current day of month
	// and add all days from prior months
	uint16_t ret = (uint16_t) cur_time.days;
	ret += (uint16_t) days[is_leap_year(cur_time.years)][cur_time.months - 1];
	return ret;
	
	/* for (int i = cur_time.months - 2; i >= 0; --i) { */
	/* 	ret += days[i]; */
	/* } */
}		

/* Day-of-year to day-of-month */
uint8_t
doy_to_dom (const uint16_t* day_of_year, const uint16_t* year)
{
	const uint16_t days[2][12] =
		{
			{ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 },
			{ 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 },
		};
	const uint8_t leap_idx = is_leap_year(*year);

	uint8_t m = 12;
	while (*day_of_year > (days[leap_idx][m - 1])) {
		--m;
	}
	return m;
}


/* static char */
/* is_leap_year(uint16_t* year) */
/* { */
/* 	if ((*year % 4) == 0) { */
/* 		if ((*year % 100) == 0) { */
/* 			if ((*year % 400) == 0) { */
/* 				// leap year */
/* 				return 1; */
/* 			} */
/* 			else { */
/* 				return 0; */
/* 			} */
/* 		} */
/* 		else { */
/* 			// leap year			 */
/* 			return 1; */
/* 		} */
/* 	} */
/* 	else { */
/* 		return 0; */
/* 	} */
/* } */

void
date_increment()
{
	switch (cur_time.months)
		{
		case 2:
			if (cur_time.days == 29) {
				cur_time.days = 1;
				cur_time.months++;
			}
			else if (cur_time.days < 28) {
				cur_time.days++;
			}
			else {
				// check for leap year
				if (is_leap_year(cur_time.years)) {
					cur_time.days = 29;
				}
				else {
					cur_time.days = 1;
					cur_time.months++;
				}
			}
			break;

		case 4:
		case 6:
		case 9:
			if (cur_time.days == 30) {
				cur_time.days = 1;

				cur_time.months++;
			}
			else {
				cur_time.days++;
			}
			break;
			
			
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
			if (cur_time.days == 31) {
				cur_time.days = 1;

				cur_time.months++;
			}
			else {
				cur_time.days++;
			}
			break;			
			
		case 12:
			if (cur_time.days == 31) {
				cur_time.days = 1;

				cur_time.months = 1;
				cur_time.years += 1;
			}
			else {
				cur_time.days++;
			}
			break;
		}
	
}

void
time_increment ()
{
	if (cur_time.secs == 59) {
		cur_time.secs = 0;

		if (cur_time.mins == 59) {
			cur_time.mins = 0;

			if (cur_time.hours == 23) {
				cur_time.hours = 0;

				date_increment();
			}
			else {
				++cur_time.hours;
			}
		}
		else {
			++cur_time.mins;
		}
	}
	else {
		++cur_time.secs;		
	}
}

/* 1 <= s <= 59 needs to be ensured!*/
void
time_increment_by(uint8_t s)
{
	cur_time.secs = (cur_time.secs + s) % 60;
	if (cur_time.secs < s) { // seconds overflow
		if (cur_time.mins == 59) {
			cur_time.mins = 0;
			if (cur_time.hours == 23) {
				cur_time.hours = 0;

				date_increment();
			}
			else {
				++cur_time.hours;
			}
		}
		else {
			++cur_time.mins;
		}
	}
	else {
		; // already calculated
	}
}

void
time_set (uint8_t s, uint8_t m, uint8_t h)
{
	cur_time.secs = s;
	cur_time.mins = m;
	cur_time.hours = h;
}

void
time_set_date (uint8_t d, uint8_t m, uint16_t y)
{
	cur_time.days = d;
	cur_time.months = m;
	cur_time.years = y;
}

void
time_fill_time_string (char* sp)
{
	// "00:00:00\0"
#if !defined(EMU) && defined(__FLASH)
	snprintf_P (sp, 9 * sizeof(char), PSTR("%02u:%02u:%02u")
#else				
	snprintf (sp, 9 * sizeof(char), "%02u:%02u:%02u"
#endif
				, cur_time.hours
				, cur_time.mins
				, cur_time.secs
				);
}

void
time_fill_date_string (char* sp)
{
	// "00.00.0000\0"
#if !defined(EMU) && defined(__FLASH)
	snprintf_P (sp, 11 * sizeof(char), PSTR("%02u/%02u/%04u")
#else
	snprintf (sp, 11 * sizeof(char), "%02u/%02u/%04u"
#endif
				, cur_time.days
				, cur_time.months
				, cur_time.years
				);
}

void
time_read_time_string (char* sp)
{
	uint8_t h = 0;
	uint8_t m = 0;
	uint8_t s = 0;
	int n = 0;
#if !defined(EMU) && defined(__FLASH)
	sscanf_P (sp, PSTR("%02u:%02u:%02u%n")
#else
	sscanf (sp, "%02u:%02u:%02u%n"
#endif			
			  , &h
			  , &m
			  , &s
			  , &n
			  );

	if (h < 24 && m < 60 && s < 60) {
		time_set (s, m, h);
	}
	else {
#if !defined(EMU) && defined(__FLASH)
		log_P ("Format error, reading time string:\n"
#else
		log ("Format error, reading time string:\n"			   
#endif
			 "\t%s\n", sp);
	}
}

void
time_read_date_string (char* sp)
{
	uint8_t d = 0;
	uint8_t m = 0;
	uint16_t y = 0;
	int n = 0;
#if !defined(EMU) && defined(__FLASH)
	sscanf_P (sp, PSTR("%02u/%02u/%04u%n")
#else
	sscanf (sp, "%02u/%02u/%04u%n"
#endif
			  , &d
			  , &m
			  , &y
			  , &n
			  );

	if (m > 0 && m < 13 && d > 0 && d < 32) {
		time_set_date (d, m, y);
	}
	else {
#if !defined(EMU) && defined(__FLASH)
		log_P ("Format error, reading date string:\n"
#else
		log ("Format error, reading date string:\n"
#endif
			   "\t n=%d\n"
			   "\t%s\n"
			   "\t%02u/%02u/%04u\n"
			   , n
			   , sp
			   , d, m, y
			   );
	}
}

/* static char */
/* time_to_epoch_2017(uint32_t* ret) { */
/* 	if (cur_time.years < 2017) { */
/* 		// error, cannot compute */
/* 		return 1; */
/* 	} */

/* 	char is_leap = is_leap_year (cur_time.years); */
		
/* 	*ret = 0; */
/* 	*ret += ((uint32_t) cur_time.secs); */
/* 	*ret += ((uint32_t) cur_time.mins)  * ((uint32_t) 60); */
/* 	*ret += ((uint32_t) cur_time.hours) * ((uint32_t) 3600); */
/* 	*ret += ((uint32_t) (cur_time.days - 1)) * ((uint32_t) (3600 * 24)); */

/* 	// TODO: check months */
/* 	switch (cur_time.months) { */
/* 	case 2: */
/* 		if (is_leap) { */
/* 			*ret += ((uint32_t) (cur_time.months - 1)) * ((uint32_t) (3600 * 24 * 29)); */
/* 		} */
/* 		else { */
/* 			*ret += ((uint32_t) (cur_time.months - 1)) * ((uint32_t) (3600 * 24 * 28)); */
/* 		} */
/* 		break; */

/* 	case 4: */
/* 	case 6: */
/* 	case 9: */
/* 		*ret += ((uint32_t) (cur_time.months - 1)) * ((uint32_t) (3600 * 24 * 30)); */
/* 		break; */

/* 	case 1: */
/* 	case 3: */
/* 	case 5: */
/* 	case 7: */
/* 	case 8: */
/* 	case 10: */
/* 	case 12: */
/* 		*ret += ((uint32_t) (cur_time.months - 1)) * ((uint32_t) (3600 * 24 * 31)); */
/* 		break; */
			
			
/* 	} */
		

/* 	// TODO: check for leap year */
/* 	if (is_leap_year (cur_time.years)) { */
/* 		*ret += ((uint32_t) (cur_time.months - 1)) */
/* 			* ((uint32_t) (29 + 31*7 + 30*3)) */
/* 			* ((uint32_t) (3600 * 24)); */
/* 	} */
/* 	else { */
/* 		*ret += ((uint32_t) (cur_time.months - 1)) */
/* 			* ((uint32_t) (28 + 31*7 + 30*3)) */
/* 			* ((uint32_t) (3600 * 24)); */
/* 	} */
/* 	return 0; */
/* } */

char
time_to_epoch_1970(uint32_t* ret)
{
	if (cur_time.years < 1970) {
		// error, cannot compute
		return 1;
	}

	// Use this calculation from POSIX!
	/* tm_sec + tm_min*60 + tm_hour*3600 + tm_yday*86400 + */
	/*   (tm_year-70)*31536000 + ((tm_year-69)/4)*86400 - */
	/*   ((tm_year-1)/100)*86400 + ((tm_year+299)/400)*86400	 */

	// :TODO: is_leap wasn't used here, should it?
	/* char is_leap = is_leap_year (cur_time.years); */
		
	*ret = 0;
	*ret += ((uint32_t) cur_time.secs);
	*ret += ((uint32_t) cur_time.mins)  * UINT32_C(60);
	*ret += ((uint32_t) cur_time.hours) * UINT32_C(3600);

	// Note, the representation of 'u_time_t' expects to be 1 - 366!
	*ret += ((uint32_t) get_current_day_of_year() - 1) * UINT32_C(86400);

	// Note, the represenation of 'u_time_t' saves the actual year
	// (not like in struct tm)
	uint32_t tm_year = cur_time.years - 1900;
	
	*ret += ( tm_year - UINT32_C(70)) * UINT32_C(31536000); // 365 days
	*ret += ((tm_year - UINT32_C(69)) / UINT32_C(4)) * UINT32_C(86400);
	*ret -= ((tm_year - UINT32_C(1)) / UINT32_C(100)) * UINT32_C(86400);
	*ret += ((tm_year + UINT32_C(299)) / UINT32_C(400)) * UINT32_C(86400);
	return 0;
}

// https://stackoverflow.com/questions/1692184/converting-epoch-time-to-real-date-time
/*

struct tm *
gmtime(register const time_t *timer)
{
        static struct tm br_time;
        register struct tm *timep = &br_time;
        time_t time = *timer;
        register unsigned long dayclock, dayno;
        int year = EPOCH_YR;

        dayclock = (unsigned long)time % SECS_DAY;
        dayno = (unsigned long)time / SECS_DAY;

        timep->tm_sec = dayclock % 60;
        timep->tm_min = (dayclock % 3600) / 60;
        timep->tm_hour = dayclock / 3600;
        timep->tm_wday = (dayno + 4) % 7;       // day 0 was a thursday
        while (dayno >= YEARSIZE(year)) {
                dayno -= YEARSIZE(year);
                year++;
        }
        timep->tm_year = year - YEAR0;
        timep->tm_yday = dayno;
        timep->tm_mon = 0;
        while (dayno >= _ytab[LEAPYEAR(year)][timep->tm_mon2]) {
                dayno -= _ytab[LEAPYEAR(year)][timep->tm_mon];
                timep->tm_mon++;
        }
        timep->tm_mday = dayno + 1;
        timep->tm_isdst = 0;

        return timep;
}
*/

			  /*
http://www.cnblogs.com/cutepig/archive/2010/05/29/1746937.html
			   */
			  
void
epoch_1970_to_u_time_t (uint32_t* src, u_time_t* dest)
{
	const uint16_t days[2][12] =
		{
			{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
			{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
		};

	*dest = (u_time_t){ 0, 0, 0, 0, 0, 0 };

	uint32_t day_num  = *src / 86400; // 'days' variable
	uint32_t day_rest = *src % 86400; // 'rem'  variable

	dest->hours = (day_rest / 3600);
	dest->mins = (day_rest % 3600) / 60;	
	dest->secs = day_rest % 60;

	uint16_t year = 1970;
	while (day_num >= get_yearsize(year)) {
		day_num -= get_yearsize(year);
		year++;
	}
	dest->years = year;

	uint8_t leap_idx = is_leap_year(year);
	uint8_t month = 0;

	//printf ("DEBUG: Day of year: %u\n", day_num);
	while (day_num >= days[leap_idx][month]) {
		day_num -= days[leap_idx][month];
		++month;
	}	
	
	dest->months = month + 1;
	dest->days = day_num + 1;
}
