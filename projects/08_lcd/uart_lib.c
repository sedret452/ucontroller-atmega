/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * uart_lib.c: UART handling module
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include "uart_lib.h"

/* The 'setbaud' library provides functionality that is used to
 *  calculate proper values for setting up BAUD.
 */
#ifndef F_CPU
  #define F_CPU 1000000UL
#else
  #warning "F_CPU was already set to value %d", F_CPU
#endif
#define BAUD 9600UL
#include <util/setbaud.h>

FILE uart_output = FDEV_SETUP_STREAM (uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE uart_input  = FDEV_SETUP_STREAM (NULL, uart_getchar, _FDEV_SETUP_READ);
//FILE uart_io     = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);


/**
 * uart_init - Initialize UART.
 *
 * Initializes UART based on UBRR value (calculated using macros).
 */
void uart_init (void)
{
#if (MCU == atmega328p)
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;

  #if USE_2X
	UCSR0A |= _BV(U2X0);
  #else
	UCSR0A &= ~(_BV(U2X0));
  #endif

	// Enable RX and TX
	UCSR0B = _BV(RXEN0) | _BV(TXEN0); 
	// Asynchronous 8N1 mode
	UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
#else
 #warning "Using alternative"
	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;
		 
	// Switch on UART TX
	UCSRB |= (1<<TXEN);

	// Asynchronous 8N1 mode
	UCSRC = (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0);
#endif

	stdout = &uart_output;
	stdin  = &uart_input;
}

int uart_putchar(char c, FILE *stream) {
	if (c == '\n') {
		uart_putchar('\r', stream);
	}
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;

	return c;
}

int uart_getchar(FILE *stream) {
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}
