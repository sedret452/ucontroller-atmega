/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * time_lib.h: Time module, typical time handling functions.
 *
 * Copyright	(C) 2013-2022 sedret452
 *
 * Note: IN avrlibc 2.0 time.h is available (see:
 * http://www.nongnu.org/avr-libc/user-manual/group__avr__time.html)
 *
 * In older versions this wasn't available -- or didn't fit the needs
 * of the projects (?) -- hence required time functions (and some
 * more) were recreated from scratch; and adjusted to the individual
 * requirements of the project.
 */

#include <inttypes.h>
 
// u_time_t: A micro controller time_t
typedef struct {
	// TODO: struct tm uses 0-60 seconds for leap second !!
	uint8_t  secs;    // 0 - 59 -> 6 bit
	uint8_t  mins;    // 0 - 59 -> 6 bit
	uint8_t  hours;   // 0 - 23 -> 5 bit
	uint8_t  days;    // 1 - 31 -> 5 bit
	uint8_t  months;  // 1 - 12 -> 4 bit
	uint16_t years;   // X - 1900 --> Could be reduced to uint8_t!
} u_time_t;

// This will *define one* global variable storing the current time.
u_time_t cur_time;

// returns 0 (no leap year), 1 (leap year)
uint8_t
is_leap_year (const uint16_t year);

uint16_t
get_yearsize (const uint16_t year);

uint16_t
get_current_day_of_year ();

void
date_increment();

void
time_increment ();

/* 1 <= s <= 59 needs to be ensured!*/
void
time_increment_by (uint8_t s);

void
time_set (uint8_t s, uint8_t m, uint8_t h);

void
time_set_date (uint8_t d, uint8_t m, uint16_t y);

void
time_fill_time_string (char* sp);

void
time_fill_date_string (char* sp);

void
time_read_time_string (char* sp);

void
time_read_date_string (char* sp);

char
time_to_epoch_1970(uint32_t* ret);

void
epoch_1970_to_u_time_t (uint32_t* src, u_time_t* dest);
