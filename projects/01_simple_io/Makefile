# SPDX-License-Identifier: GPL-2.0-only

# Makefile for simple ATmega32-16PU (or ATmega328P-PU) projects
#
# ATmega32-16PU, 8-bit Microcontroller,
#   -  Atmel AVR instruction set architecture (ISA): avr5 [GCC naming]

# Tool variables (start)

#####
## Use for ATmega32-16PU
#MCU = atmega32
#AVRDUDE_MCU = m32

## Use for ATmega328P-PU
MCU = atmega328p
AVRDUDE_MCU = atmega328p
#####

AVRDUDE = /usr/bin/avrdude -vvv

## Using MyMultiprog with MyUSBProg
## avr910 works for ATmega32-16PU
#AVRDUDE_PROGRAMMER = avr910  

## Using MyMultiprog with MyUSBProg
## avr911 works for ATmega328P-PU
AVRDUDE_PROGRAMMER = avr911

AVRDUDE_PORT = /dev/ttyUSB0
CC = /usr/bin/avr-gcc
CFLAGS += -mmcu=$(MCU) -Os
OBJCOPY = /usr/bin/avr-objcopy
OBJDUMP = /usr/bin/avr-objdump
RM = /bin/rm -f
# Tool variables (end)

PROJECT = simple_io

$(PROJECT).elf: $(PROJECT).c
	$(CC) $(CFLAGS) -o $@ $<

$(PROJECT).elf.hex: $(PROJECT).elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

# Create an assembler source file from project's source file
$(PROJECT).asm: $(PROJECT).c
	$(CC) -S $(CFLAGS) -o $@ $<

# Create an extended assembler listing from object file
$(PROJECT).lst: $(PROJECT).c
	$(OBJDUMP) -h -S $< > $@

upload: $(PROJECT).elf.hex
	sudo $(AVRDUDE) -p $(AVRDUDE_MCU) -e -c $(AVRDUDE_PROGRAMMER) -Uflash:w:$(PROJECT).elf.hex -P $(AVRDUDE_PORT)

clean:
	$(RM) $(PROJECT).hex $(PROJECT).elf $(PROJECT).asm $(PROJECT).lst $(PROJECT).elf.hex

.PHONY: clean
