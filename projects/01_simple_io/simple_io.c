/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * simple_io.c: Simple I/O example for atmega328p, LEDs on I/O ports.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include <avr/io.h>

int main (void)
{
	unsigned char was_set = 0;
	// Set port D's Pin 
	DDRC  = 0xFE; // set Port D, all pins as output except pin0 as input
	PORTC = 0x01; // set all output pins to 0, and set pin0 (input) to pull-up

	for (;;) {
		// If input pin (pin0) set, then switch pin1 and pin2
		if (bit_is_clear (PINC, 0)) {
			if (!was_set) {
				PORTC |= _BV(1);
				PORTC &= ~(_BV(2));
				was_set = 1;
			}
			else {
				PORTC |= _BV(2);
				PORTC &= ~(_BV(1));
				was_set = 0;
			}
		}
	}

	return 0;
}
