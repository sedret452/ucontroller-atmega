/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * uart.c: Simple usage example for UART based text I/O.
 *
 * Based on desciptions, as given here:
 *
 *  - https://www.mikrocontroller.net/articles/AVR-GCC-Tutorial/Der_UART
 *  - http://www.appelsiini.net/2011/simple-usart-with-avr-libc
 *
 * Note: In later projects, this was externalized into uart_lib
 * module.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include <avr/io.h>

#include <stdlib.h>
#include <stdio.h>

#include <ctype.h>


#ifndef F_CPU
  #define F_CPU 1000000UL
#else
  #warning "F_CPU was already set to value %d", F_CPU
#endif

#define BAUD 9600UL

/* The 'setbaud' library provides functionality that is used to
 *  calculate proper values for setting up BAUD.
 */
#include <util/setbaud.h>

/**
 * uart_init - Initialize UART.
 *
 * Initializes UART based on UBRR value (calculated using macros).
 */
void uart_init (void)
{
#if (MCU == atmega328p)
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;

  #if USE_2X
	UCSR0A |= _BV(U2X0);
  #else
	UCSR0A &= ~(_BV(U2X0));
  #endif

	// Enable RX and TX
	UCSR0B = _BV(RXEN0) | _BV(TXEN0); 
	// Asynchronous 8N1 mode
	UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
#else
 #warning "Using alternative"
	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;
		 
	// Switch on UART TX
	UCSRB |= (1<<TXEN);

	// Asynchronous 8N1 mode
	UCSRC = (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0);
#endif
}

/**
 * uart_putc - Putc for UART
 *
 * TODO:
 *   -  This is depending on used controller
 *      Check this for ATmega328 to work
 */
//void uart_putc (unsigned char c)
//{
//#if (MCU == atmega328p)
//	/* Wait until ready to send */
//	loop_until_bit_is_set(UCSR0A, RXC0);
//	UDR0 = c;
//#else
//	/* Wait until ready to send */
//	while (!(UCSRA & (1<<UDRE))) {
//		;
//	}                             
//
//	UDR = c;
//#endif
//}

/**
 * uart_puts - Puts for UART
 */
//void uart_puts (char *s)
//{
//	while (*s) {
//		uart_putc(*s);
//		s++;
//	}
//}


/**
 * uart_getc - getc for UART
 *
 *   TODO:
 *   -  This works in Blocking mode
 */
//uint8_t uart_getc(void)
//{
//#if (MCU == atmega328p)
//	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
//	return UDR0;
//#else
//	/* Wait until there's a character available */
//	while (!(UCSRA & (1<<RXC))) {
//		;
//	}
//
//	return UDR;
//#endif
//}

/**
 * uart_gets - gets for UART
 */
//void uart_gets (char* buf, uint8_t len)
//{
//	uint8_t c = 0;
//	uint8_t cur_len = 0;
//
//	c = uart_getc ();
//	while ( (c != '\n') && (cur_len < (len - 1)) ) {
//		*buf = c;
//		buf++;
//		cur_len++;
//		c = uart_getc ();
//	}
//
//	*buf = '\0';
//}

void uart_putchar(char c, FILE *stream) {
	if (c == '\n') {
		uart_putchar('\r', stream);
	}
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
}

char uart_getchar(FILE *stream) {
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

int read_line (char* input_buf, int max_len, FILE* stream)
{
	int c = EOF;
	char* p = input_buf;
	int cur_len = max_len;
	

	if (cur_len < 2)
		return -1;
	
	for (;;) {
		c = getc (stream);
		if (c == EOF) {
			*p = '\0';
			break;
		}

		if (c == '\n' || c == '\r') {
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			break;
		}
		
		// echo c
		if (!iscntrl (c)) {
			*p = (char) c;
			p++;
			cur_len--;
			
			putchar (c);
		}
		else {
			// TODO:
			//  - 0x04 (end of transmission -- (execute exit)
			//  - Use '\f' analog to '\n'
			switch (c) {
				// form feed
			case '\f':
				putchar ('\f');
				break;
				// back space
			case '\b':
				if ( (p - input_buf) != 0 ) {
					p--;
					cur_len--;
					putchar ('\b');
				}				

				break;				
			default:
				// DEBUG output
				printf ("%x", c);
				break;
				
			}
		}

		if (cur_len <= 1) {
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			break;
		}

	}
	
	return 0;
}

FILE uart_output = FDEV_SETUP_STREAM (uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE uart_input  = FDEV_SETUP_STREAM (NULL, uart_getchar, _FDEV_SETUP_READ);
//FILE uart_io     = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);

int main (void)
{
	unsigned char was_set = 0;
	// Set port C's Pin 
	DDRC  = 0xFE; // set Port C, all pins as output except pin0 as input
	PORTC = 0x01; // set all output pins to 0, and set pin0 (input) to pull-up

#define LINE_LEN 80

	char input_buf[LINE_LEN];

	uart_init();
	stdout = &uart_output;
	stdin  = &uart_input;

	for (;;) {
		fputs ("# ", stdout);
		//fgets (input_buf, sizeof (input_buf) - 1, stdin);
 		read_line (input_buf, sizeof (input_buf) - 1, stdin);
		printf ("Debug: %s\n", input_buf);

		if (!was_set) {
			PORTC |= _BV(1);
			PORTC &= ~(_BV(2));
			was_set = 1;
		}
		else {
			PORTC |= _BV(2);
			PORTC &= ~(_BV(1));
			was_set = 0;
		}

	}

	return 0;
}
