/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * mcp9700a_lib.h: Temperature sensor module (Microchip 9700a)
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#ifndef __MCP9700A_LIB_H_
#define __MCP9700A_LIB_H_

#if (MCU != atmega328p)
 #error "mcp9700a_lib is only usable for ATmega328p!  Be sure to set properly MCU variable."
#endif

#include <inttypes.h>

void    mcp9700a_init (void);
uint8_t mcp9700a_read_temp (uint16_t* t);

extern uint8_t reference_voltage; // Voltage * 10 (e.g. 3.3V => 33)

#endif /* __MCP9700A_LIB_H_ */
