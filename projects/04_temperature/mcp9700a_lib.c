/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * mcp9700a_lib.c: Temperature sensor module (Microchip 9700a)
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include "mcp9700a_lib.h"
#include <avr/io.h>
#include "uart_lib.h"
#include "log_lib.h"

#include <avr/pgmspace.h>

#include <stdint.h>

uint8_t reference_voltage = 33;

#define MCP9700A_PIN_IN  PINB5
#define MCP9700A_PIN     PB5

void mcp9700a_init (void)
{
	// enable Analog and choose prescale divisor to 8
	//ADCSRA = 1<<ADEN | 1<<ADPS1 | 1<<ADPS0;

	// Enable ADC and choose the ADC prescaler to be 16.  It is
	// assumed that an internal CPU frequency of 1MHz is used, this
	// makes a 62.5 kHz ADC sampling rate.  Which should be OK, since
	// the temperature value won't change any fast.
	ADCSRA = 1<<ADEN | 1<<ADPS2;
}

uint8_t
mcp9700a_read_temp (uint16_t* t)
{
	uint8_t high = 0;
	uint8_t low  = 0;

	// ADPS1 | ADPS0 => Choose prescalser: 8
	//ADCSRA = 1<<ADEN | 1<<ADPS1 | 1<<ADPS0;

	// Choose temperature sensor, leave everything else
	//AMUX = (AMUX & 0b11110000) | 0b1000; 
	
	ADCSRA &= ~(1<<PRADC);	// Prepare for single conversion mode
							
	ADCSRA |= 1<<ADSC;      // Start single conversion mode
	loop_until_bit_is_set(ADCSRA,ADSC);
	
	low  = ADCL;
	high = ADCH;
	
	ADCSRA |= (1<<PRADC); // Switch on power reduction ADC, again

	/* low = 0xF; */
	/* high = 0xD; */

	//ADCSRA &= ~_BV(ADIE);

    log_debug_P ("low: 0x%x, high: 0x%x: 10-bit: 0x%x\n", low, high, (high << 8 | low));

	uint16_t measured_val = (uint16_t) (high << 8 | low);
	log_debug_P ("Measured val: %u \n", measured_val);


	// V_in = (ADC * V_ref) / 1024
	//        (val * 3.3) / 1024
	//
	//

	// !! This part works, using floating point
	//uint16_t val_ = (((uint16_t) (high << 8 | low)) * 33) / 10.24;

    uint64_t val = (measured_val * ((uint64_t) reference_voltage) * 10000000000ll) / 1024ll;
    uint64_t correction = (535ll * 100000000ll);

	uint64_t res = val - correction;

	uint16_t t_cutted = (res / 100000000ll);  // 29,6 °C => 296
	log_debug_P ("Temp = %u (29,6 C => 296)\n", t_cutted);

	*t = t_cutted;
	
	//
	//   V_out = T_c * T_a + V_0°C
	//
	//   T_c = Temperature coefficient (typical: 10.0mV / 1°C)
	//
	//   T_a = Ambient temperature
	//
	//   V_0°C = typical: 500mV => Maybe on the sensor 535mV
	//
	//   Problem: How to determine T_c correctly?

	// !! This part works, using floating point
	//printf ("Measured V_in: %u mV (dec)\n", val_);
	//*t = (val_ - 535);
	
	return 1;
}
