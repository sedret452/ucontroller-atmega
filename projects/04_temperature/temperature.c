/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * temperature.c: Sensor node. Reads and stores sensor values into
 * external storage.
 *
 * Sensor values can be read-out directly or stored into an externally
 * connected storage (RAM or EEPROM).  To allow for timestamping, a
 * (software) clock is provided too.
 *
 * It is possible to handle the sensor in a manual way, as also to
 * configure the execution of a time-triggered series of measurements.
 *
 * A rather complex CLI is provided, see the definitions of:
 *   cmd_..._usage[]
 * to get a grasp.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

/*

  TODO
  ----

  There's an internal temperature sensor directly on ATmega328P
  available.  What is necessary to be able to use this one?

  - Choose correct mux channel (how to configure ADMUX?)

  - Enable ADC

    - Set ADEN bit in ADCSRA register

  - The 10-bit result will be put into ADC data registers (ADCH and
    ADCL) right adjusted

    - Read ADCL *first*, then read ADCH!

  - Single conversion

    - Started by clearing PRADC bit (actually Power Reducion ADC)

    - First write 0 into PRADC bit, Second write 1 to ADSC

    - ADSC will turn 0 again, when the single conversion is finished

  - Muxer for temperature sensor configuration

    - ADMUX |= 0b1000; // 0x08 -> Temperature sensor


  TODO: 2016-06-25

  - The biggest problems for this project -- right now -- is to have a
    proper reference voltage.  It seems that the voltage regulators
    aren't to stable (the ones I'm using on the bread board).  The
    reference voltage ranges from 3.6V to 3.12V.

    - Why is that?

    - the 33ll value for the calculation should be runtime adjustable!

  - Read "AVR353" (doc8060.pdf).  This says something about reference
    voltage for ADC, including temperature sensor (the internal one?).

  TODO: 2016-06-26

  Advanced circuit for this board:

  a. Use a 9V battery block and a constant voltage regulator (to 3.3V)

  b. Provide a dip switch (as a digital input to atmega328p that makes
  it possible to switch on, switch off UART interface (this may save
  power).

  c. Provide code that can be started via UART interface and makes it
  possible to start a Messreihe!
   
*/


#include <avr/sleep.h>

#include <stdlib.h>
#include <stdio.h>

#include <string.h> // strcmp()
#include <inttypes.h>

#ifndef EMU
 #include <avr/io.h>
 #include "uart_lib.h"
#endif

#include "log_lib.h"
#include "m93c86_lib.h"
#include "mcp9700a_lib.h"


#include <ctype.h> /* for iscntrl() */
void read_line (char* input_buf, int max_len, FILE* stream);

static const __flash char system_info_str[] =
	"temperature" " " "v0.2" "\n"
	"  m93c86, mcp9700a\n";

typedef uint16_t msa_t;
msa_t msa = 0;

// :TODO: This is still statically configured for m93c86
// :TODO: Search for 1023 and 1024!
static const msa_t msa_max = 1023;

typedef struct {
	uint32_t timestamp;
	uint16_t temperature;
} measurement_t;

typedef uint16_t msa_w_time_t;
msa_w_time_t msat = 0;

// For m93c86 2048 addresses exist, from 0 to 2047.  Therefore, the
// number of address for a full measurement (6 bytes) are:
// 2048/6 = 341 -- with 2 bytes left unused).
// Finally, the 'msat' address range is 0 to 340 
static const msa_w_time_t msa_w_time_max =
	((eeprom_mem_address_max + 1) / sizeof(measurement_t)) - 1;

enum _cmd_t {
	cmd_read_byte = 0
	, cmd_write_byte = 1
	, cmd_init = 2
	, cmd_set_write_enable = 3
	, cmd_set_write_disable = 4
	, cmd_help = 5
	, cmd_error = 6
	, cmd_read_byte_multi = 7
	, cmd_read_temperature = 8
	, cmd_get_reference_voltage = 9
	, cmd_set_reference_voltage = 10
	, cmd_get_measurements_start_address = 11
	, cmd_set_measurements_start_address = 12
	, cmd_run = 13
	, cmd_get_tmem = 14
	, cmd_set_tmem = 15
	, cmd_run_write = 16
	, cmd_isr_set = 17
	, cmd_isr_unset = 18
	, cmd_info = 19
	, cmd_write_measurement = 20
	, cmd_read_measurement = 21
	, cmd_get_msat = 22
	, cmd_set_msat = 23
	, cmd_time_get = 24
	, cmd_time_put = 25
	, cmd_date_get = 26
	, cmd_date_put = 27
	
};
typedef enum _cmd_t cmd_t;

#define ONLINE_HELP
#ifdef ONLINE_HELP

#if defined __FLASH

#include <avr/pgmspace.h>

static const __flash char cmd_init_usage [] =
	"init - (re-)initialize SPI\n"
	"\tusage: init\n";
static const __flash char cmd_info_usage [] =
	"info - get system info\n"
	"\tusage: info\n";
static const char __flash cmd_read_byte_usage[] =
	"get byte - read from memory address (byte mode)\n"
	"\tusage: get byte <address>\n";
static const __flash char cmd_write_byte_usage[] =
	"put byte - write to memory address (byte mode)\n"
	"\tusage: put byte <address> <value>\n";
static const __flash char cmd_set_write_enable_usage[] =
	"put wenb - set write enable\n"
	"\tusage: put wenb\n";
static const __flash char cmd_set_write_disable_usage[] =
	"put wdis - set write disable\n"
	"\tusage: put wdis\n";

static const char __flash cmd_read_byte_multi_usage[] =
	"get mult - read multiples from memory address (byte mode), page\n"
	"\tusage: get mult <address>\n";

static const char __flash cmd_read_temperature_usage[] =
	"get temp - read temperature from sensor\n"
	"\tusage: get temp\n";

static const char __flash cmd_set_reference_voltage_usage[] =
	"put ref - set reference voltage\n"
	"\tusage: put ref <integer-value>\n";

static const char __flash cmd_get_reference_voltage_usage[] =
	"get ref - get current reference voltage\n"
	"\tusage: get ref\n";

// Measurement start address must be MSA % 2 = 0
static const char __flash cmd_get_measurements_start_address_usage[] =
	"get msa - get Measurement Start Address\n"
	"\tusage: get msa\n";

static const char __flash cmd_set_measurements_start_address_usage[] =
	"put msa - put Measurement Start Address\n"
	"\tusage: put msa <address>\n";

static const char __flash cmd_run_measurements_store_usage[] =
	"run senb - run measurements (n times, starting at cur MSA), store enabled\n"
	"\tusage: run senb <n>\n";

static const char __flash cmd_run_measurements_usage[] =
	"run sdis - run measurements (n times, starting at cur MSA), store disabled\n"
	"\tusage: run sdis <n>\n";

static const char __flash cmd_get_tmem_usage[] =
	"get tmem - get temperature at current MSA\n"
	"\tusage: get tmem\n";

static const char __flash cmd_put_tmem_usage[] =
	"put tmem - put current temperature to current MSA\n"
	"\tusage: put tmem\n";

static __flash const char cmd_isr_set_usage [] =
	"isrs - ISR set\n"
	"\tusage: isrs\n";

static __flash const char cmd_isr_unset_usage [] =
	"isru - ISR unset\n"
	"\tusage: isru\n";

static const char __flash cmd_read_measurement_usage[] =
	"get val - get measurement from current MSA\n"
	"\tusage: get val\n";

static const char __flash cmd_write_measurement_usage[] =
	"put val - put measurement to current MSA\n"
	"\tusage: put val\n";

static const char __flash cmd_get_msat_usage[] =
	"get msat - get Measurement Start Address (with time)\n"
	"\tusage: get msat\n";

static const char __flash cmd_put_msat_usage[] =
	"put msat - put Measurement Start Address (with time)\n"
	"\tusage: put msat <address>\n";

static __flash const char cmd_time_get_usage [] =
	"get time - Read current time\n"
	"\tusage: get time\n";

static __flash const char cmd_date_get_usage [] =
	"get date - Read current date\n"
	"\tusage: get date\n";

static __flash const char cmd_time_put_usage [] =
	"put time - Read in time\n"
	"\tusage: put time \"00:00:00\"\n";

static __flash const char cmd_date_put_usage [] =
	"put date - Read in date\n"
	"\tusage: put date \"00/00/0000\"\n";


static __flash const char* usage_vec[] = {
	cmd_read_byte_usage
	, cmd_write_byte_usage
	, cmd_init_usage
	, cmd_info_usage
	, cmd_set_write_enable_usage
	, cmd_set_write_disable_usage
	, cmd_read_byte_multi_usage
	, cmd_read_temperature_usage
	, cmd_set_reference_voltage_usage
	, cmd_get_reference_voltage_usage
	, cmd_set_measurements_start_address_usage
	, cmd_get_measurements_start_address_usage
	, cmd_run_measurements_usage
	, cmd_run_measurements_store_usage
	, cmd_isr_set_usage
	, cmd_isr_unset_usage
	, cmd_read_measurement_usage
	, cmd_write_measurement_usage
	, cmd_get_msat_usage
	, cmd_put_msat_usage
	, cmd_time_get_usage
	, cmd_time_put_usage
	, cmd_date_get_usage
	, cmd_date_put_usage
};

#elif defined EMU

 #error "No emulation supported for m93c86 library"

};
#else

 #error "No flash namespace available"

#endif /* FLASH , EMU */
#endif /* ONLINE_HELP */

#ifdef ONLINE_HELP

int
print_usage (cmd_t cmd)
{
	if (cmd == cmd_error) {
#ifdef __FLASH

		// I know, %S has usually a different meaning, so lets turn
		// off type warnings here
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
		
		/* printf ("%S", cmd_init_usage); */
		/* printf ("%S", cmd_read_byte_usage); */
		/* printf ("%S", cmd_write_byte_usage); */
		/* printf ("%S", cmd_set_write_enable_usage); */
		/* printf ("%S", cmd_set_write_disable_usage); */
		/* printf ("%S", cmd_read_byte_multi_usage); */
		/* printf ("%S", cmd_read_temperature_usage); */
		/* printf ("%S", cmd_get_reference_voltage_usage); */
		/* printf ("%S", cmd_set_reference_voltage_usage); */
		/* printf ("%S", cmd_get_measurements_start_address_usage);		 */
		/* printf ("%S", cmd_set_measurements_start_address_usage); */
		/* printf ("%S", cmd_run_measurements_usage); */
		/* printf ("%S", cmd_run_measurements_store_usage); */
		/* printf ("%S", cmd_get_tmem_usage); */
		/* printf ("%S", cmd_put_tmem_usage); */

		printf ("%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S%S"
				, cmd_init_usage
				, cmd_info_usage
				, cmd_read_byte_usage
				, cmd_write_byte_usage
				, cmd_set_write_enable_usage
				, cmd_set_write_disable_usage
				, cmd_read_byte_multi_usage
				, cmd_read_temperature_usage
				, cmd_get_reference_voltage_usage
				, cmd_set_reference_voltage_usage
				, cmd_get_measurements_start_address_usage
				, cmd_set_measurements_start_address_usage
				, cmd_run_measurements_usage
				, cmd_run_measurements_store_usage
				, cmd_get_tmem_usage
				, cmd_put_tmem_usage
				, cmd_isr_set_usage
				, cmd_isr_unset_usage
				, cmd_read_measurement_usage
				, cmd_write_measurement_usage
				, cmd_get_msat_usage
				, cmd_put_msat_usage
				, cmd_time_get_usage
				, cmd_time_put_usage
				, cmd_date_get_usage
				, cmd_date_put_usage
				);
		
		
#pragma GCC diagnostic pop		
		
#else
		log ("%s\n%s\n%s\n%s\n%s\n%s\n%s"
			 ,cmd_init_usage, cmd_read_byte_usage, cmd_write_byte_usage
			 ,cmd_set_write_enable_usage, cmd_set_write_disable_usage
			 ,cmd_read_byte_multi_usage
			 ,cmd_read_temperature_usage
			 ,cmd_set_reference_voltage_usage
			 ,cmd_get_reference_voltage_usage
			 );
#endif /* __FLASH */		
	}
	else {
#ifdef __FLASH
		// TODO, stopped working after switching to printf ("%S",...)
		printf ("%S\n", usage_vec[cmd]);
#else
		log ("%s", (usage_vec[cmd])); // This works!  At least in EMU mode ;)
#endif
	}
	return 0;
}
#endif /* ONLINE_HELP */

#ifndef EMU
#include <avr/interrupt.h>

#include "time_lib.h"

void activate_timer2();
void deactivate_timer2();

// Used as a global variable, to mark that the timer interupt had
// happen
int got_interrupted = 0;

uint8_t isr_counter = 0;
const uint8_t isr_counter_max = 10; // 6s * 10 = 60s = 1min

int eeprom_write_measurement (eeprom_mem_address addr, const measurement_t m);


/*
 * Provides a interupt handler. Here, 'TIMER2_COMPA_vect' is used
 * since Compare Overflow Mode is used.  If Normal mode (timer/counter
 * mode) was used 'TIMER2_OVF_vect' needed to be used.
 */
ISR(TIMER2_COMPA_vect)
{
	time_increment_by(6);
	isr_counter += 1;
	if (! (isr_counter < isr_counter_max)) {
		measurement_t m = { 0 };
		uint32_t now = 0;
		uint16_t t = 0;
		/* eeprom_mem_address addr = ((eeprom_mem_address) msat) * 6; */
			
		mcp9700a_read_temp (&t);
		mcp9700a_read_temp (&t);
		time_to_epoch_1970 (&now);

		m.temperature = t;
		m.timestamp = now;
		/* eeprom_write_measurement (addr, m); */
		eeprom_write_measurement (((eeprom_mem_address) msat) * 6, m);

		char time_str[] = "23:45:00";
		char date_str[] = "04/02/2018";
		time_fill_time_string (time_str);
		time_fill_date_string (date_str);
		log_P ("temperature: %d/10 C\n"
			   "  at: %s %s"
			   "  was written to: %u\n"
			   , t
			   , date_str
			   , time_str
			   , ((eeprom_mem_address) msat) * 6);

		log_P ("msat: %u\n", msat);

		msat = (msat + 1) % (msa_w_time_max + 1);
		isr_counter = 0; // reset counter
	}
}	
#endif /* not defined EMU */


void
read_line (char* input_buf, int cur_len, FILE* stream)
{
	int c = EOF;
	char* p = input_buf;

	*p = '\0'; // safe initialization
	
	if (cur_len < 2)
		return;
	
	for (;;) {
		c = getc (stream);
		
		switch (c) {
			
		case EOF:
			*p = '\0';
			return;
			
		// TODO:
		//  - 0x04 (end of transmission -- (execute exit)
		//  - Use '\f' analog to '\n'
		case '\n':
		case '\r':
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
			
		case '\f': // form feed
			putchar ('\f');
			break;
			
		case '\b': // back space
			if ( (p - input_buf) != 0 ) {
				p--;
				cur_len++;
				putchar ('\b');
				putchar (' ');
				putchar ('\b');
			}
			break;
			
		default:
			if (iscntrl (c)) {
				// DEBUG output
				log_warn_P ("Unknown control sequence: %x", c);
			}
			else {// echo character and write into buffer
				*p = (char) c;
				p++;
				cur_len--;
				putchar (c);
			}
		}
		
		if (cur_len <= 1) {
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
		}
	}	
	return;
}

int
eat_byte_val (char** input, uint8_t* byte_val)
{
	int n = 0;
	/* int eat = sscanf (*input, "%u"SCNu8, *byte_val); */
	int eat = sscanf_P (*input, PSTR("%hhd%n"), byte_val, &n);
	
	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}

int
eat_mem_address (char** input, eeprom_mem_address* addr)
{
	int n = 0;
	/* int eat = sscanf (*input, "u%"SCNu16"%n", addr, &n); */
	int eat = sscanf_P (*input, PSTR("%hd%n"), addr, &n);

	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}

int
eat_msa (char** input, msa_t* addr)
{
	int n = 0;
	/* int eat = sscanf (*input, "u%"SCNu16"%n", addr, &n); */
	int eat = sscanf_P (*input, PSTR("%hd%n"), addr, &n);

	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}


int
eat_cmd (char** input)
{
	char _cmd_1[4 + 1] = ""; // cmd first part get, put, init, help (length 4 + 1)
	char _cmd_2[4 + 1] = ""; // cmd second part (max length 4 + 1)
	int n = 0;
	
	sscanf_P (*input, PSTR("%s%n"), _cmd_1, &n);
	
	if (!strncmp_P (_cmd_1, PSTR("init"), 4)) {
		*input += n;
		return cmd_init;
	}
	else if (!strncmp_P (_cmd_1, PSTR("info"), 4)) {
		*input += n;
		return cmd_info;
	}
	else if (!strncmp_P (_cmd_1, PSTR("isrs"), 4)) {
		*input += n;
		return cmd_isr_set;
	}
	else  if (!strncmp_P (_cmd_1, PSTR("isru"), 4)) {
		*input += n;
		return cmd_isr_unset;
	}
	else if (!strncmp_P (_cmd_1, PSTR("get"), 3)) {
		*input += n;
		sscanf_P (*input, PSTR("%s%n"), _cmd_2, &n);
		
		if (!strncmp_P (_cmd_2, PSTR("byte"), 4)) {
			*input += n;
			return cmd_read_byte;
		}
		else if (!strncmp_P (_cmd_2, PSTR("mult"), 4)) {
			*input += n;
			return cmd_read_byte_multi;
		}
		else if (!strncmp_P (_cmd_2, PSTR("msat"), 4)) {
			*input += n;
			return cmd_get_msat;
		}
		else if (!strncmp_P (_cmd_2, PSTR("msa"), 3)) {
			*input += n;
			return cmd_get_measurements_start_address;
		}
		else if (!strncmp_P (_cmd_2, PSTR("ref"), 3)) {
			*input += n;
			return cmd_get_reference_voltage;
		}
		else if (!strncmp_P (_cmd_2, PSTR("temp"), 4)) {
			*input += n;
			return cmd_read_temperature;
		}
		else if (!strncmp_P (_cmd_2, PSTR("tmem"), 4)) {
			*input += n;
			return cmd_get_tmem;
		}
		else if (!strncmp_P (_cmd_2, PSTR("val"), 3)) {
			*input += n;
			return cmd_read_measurement;
		}
		else if (!strncmp_P (_cmd_2, PSTR("time"), 4)) {
			*input += n;
			return cmd_time_get;
		}
		else if (!strncmp_P (_cmd_2, PSTR("date"), 4)) {
			*input += n;
			return cmd_date_get;
		}
		else {
			// error
			return cmd_error;
		}
	}
	else if (!strncmp_P (_cmd_1, PSTR("put"), 3)) {
		*input += n;

		sscanf_P (*input, PSTR("%s%n"), _cmd_2, &n);
		if (!strncmp_P (_cmd_2, PSTR("byte"), 4)) {
			*input += n;
			return cmd_write_byte;
		}
		else if (!strncmp_P (_cmd_2, PSTR("msat"), 4)) {
			*input += n;
			return cmd_set_msat;
		}
		else if (!strncmp_P (_cmd_2, PSTR("msa"), 3)) {
			*input += n;
			return cmd_set_measurements_start_address;
		}
		else if (!strncmp_P (_cmd_2, PSTR("ref"), 3)) {
			*input += n;
			return cmd_set_reference_voltage;
		}
		else if (!strncmp_P (_cmd_2, PSTR("tmem"), 4)) {
			*input += n;
			return cmd_set_tmem;
		}
		else if (!strncmp_P (_cmd_2, PSTR("wdis"), 4)) {
			*input += n;
			return cmd_set_write_disable;
		}
		else if (!strncmp_P (_cmd_2, PSTR("wenb"), 4)) {
			*input += n;
			return cmd_set_write_enable;
		}
		else if (!strncmp_P (_cmd_2, PSTR("val"), 3)) {
			*input += n;
			return cmd_write_measurement;
		}
		else if (!strncmp_P (_cmd_2, PSTR("time"), 4)) {
			*input += n;
			return cmd_time_put;
		}
		else if (!strncmp_P (_cmd_2, PSTR("date"), 4)) {
			*input += n;
			return cmd_date_put;
		}
		else {
			// error
			return cmd_error;
		}
	}
	else if (!strncmp_P (_cmd_1, PSTR("run"), 3)) {
		*input += n;
		sscanf_P (*input, PSTR("%s%n"), _cmd_2, &n);
		
		if (!strncmp_P (_cmd_2, PSTR("senb"), 4)) {
			*input += n;
			return cmd_run_write;
		}
		else if (!strncmp_P (_cmd_2, PSTR("sdis"), 3)) {
			*input += n;
			return cmd_run;
		}
		else {
			// error
			return cmd_error;
		}
	}
#ifdef ONLINE_HELP
	else if (!strncmp_P (_cmd_1, PSTR("help"), 4)) {
		*input += n;
		return cmd_help;
	}
#endif /* ONLINE_HELP */

	else {
		// error
		return cmd_error;
	}
}

/*
 * msa:  0 <= msa <= 1023
 */
int
eeprom_write_temperature (msa_t msa, uint16_t t)
{
	if (msa <= msa_max) {
		eeprom_mem_address mem_addr =  msa * 2;
		uint8_t v[2];
		
		v[1] = (uint8_t) (t & 0x00ff);
		v[0] = (uint8_t) ( (t & 0xff00) >>  7);

		log_P ("Writing temperature to EEPROM: %u (t): %u (msa) : %u (addr)\n" 
			   "  v[0] %x, v[1] %x\n"
			   , t, msa, mem_addr
			   , v[0], v[1]
			   );
		eeprom_write_byte(mem_addr, v[0]);
		eeprom_write_byte(mem_addr + 1, v[1]);
		return 0;
	}
	else {
		log_1_P ("Error: ! MSA >= 1024!\n)");
		return -1;
	}
}

int
eeprom_read_temperature (uint16_t* t_return, msa_t msa)
{
	if (msa <= msa_max) {
		eeprom_mem_address mem_addr =  msa * 2;
		uint8_t v[2];
		eeprom_read_byte_multi(v, 2, mem_addr);
		*t_return = ((((uint16_t)v[0]) << 7) | v[1]);
		log_P ("Read temperature from EEPROM: %u (t): %u (msa) : %u (addr)\n"
			   , *t_return, msa, mem_addr);
		return 0;
	}
	else {
		log_1_P ("Error: ! MSA < 1024!\n)");
		return -1;
	}
}

int
eeprom_write_measurement (eeprom_mem_address addr, const measurement_t m)
{
	// :TODO: Check for maximum address
	
	log_debug_P ("Writing timestamp: 0x%08X\n", m.timestamp);
	log_debug_P ("Writing temperature: 0x%04X\n", m.temperature);
	
	uint8_t buf[6] = { 0 };

	buf[5] = (uint8_t) ((((uint32_t) m.timestamp) & 0xff000000) >> 23);
	buf[4] = (uint8_t) ((((uint32_t) m.timestamp) & 0x00ff0000) >> 15);
	buf[3] = (uint8_t) ((((uint32_t) m.timestamp) & 0x0000ff00) >> 8);	
	buf[2] = (uint8_t)  (m.timestamp & 0x000000ff);

	buf[1] = (uint8_t) ((m.temperature & 0xff00) >> 7);
	buf[0] = (m.temperature & 0x00ff);

	for (int i = 0; i < 6; i++) {
		eeprom_write_byte (addr + i, buf[i]);
	}
	return 0;
}

int
eeprom_read_measurement (eeprom_mem_address addr, measurement_t* m)
{
	// :TODO: Check for maximum address

	uint8_t buf[6] = { 0 };
	eeprom_read_byte_multi (buf, 6, addr);

	log_debug_P ("Read from memory:\n"
				 "addr: 0x05 0x04 0x03 0x02 0x01 0x00\n"
				 "val:  0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X\n"
				 , buf[5], buf[4], buf[3], buf[2], buf[1], buf[0]);

	m->timestamp = (((uint32_t) buf[5]) << 23) | (((uint32_t) buf[4]) << 15) | (((uint32_t) buf[3]) << 8) | (uint32_t) buf[2];
	m->temperature = (buf[1] << 8) | buf[0];

	log_debug_P ("Read from memory, timestamp:   0x%08X\n", m->timestamp);
	log_debug_P ("Read from memory, temperature: 0x%08X\n", m->temperature);

	
	
	return 0;
}


int
parse_cmd_line (char** input_buf)
{
	char* p_input_buf = *input_buf;
	cmd_t cmd = cmd_error;
	eeprom_mem_address    mem_addr = 0;

	// TODO
	/* eeprom_mem_address* p_mem_addr = &mem_addr; */
	uint8_t    byte_val = 0;
	// TODO
	/* uint8_t* p_byte_val = &byte_val; */
	uint8_t i = 0;

	uint8_t page[16];

	log_debug_P ("before *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);
	cmd = eat_cmd (&p_input_buf);
	log_debug_P ("after *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);

	if (cmd == cmd_error) {
		log_P ("Error: Wrong cmd:\n  %s\n", p_input_buf);
		return -1;
	}

	switch (cmd) {
#ifdef ONLINE_HELP
	case cmd_help:
		{
			cmd_t help_for_cmd = eat_cmd (&p_input_buf);
			print_usage (help_for_cmd);
		}
		break;
#endif /* ONLINE_HELP */
		
	case cmd_init:
		log_info_1_P ("Initialization\n");
		eeprom_init_spi();
		
		break;

	case cmd_info:
		log_info_P ("%S\n", system_info_str);
		log_info_P ("  max EEPROM address: %u\n"
					"  max measurement address: %u\n"
					"  sizeof(measurement_t): %u\n"
					, eeprom_mem_address_max
					, msa_w_time_max
					, sizeof(measurement_t)
					);
		break;
		
	case cmd_read_byte:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			log_1_P ("Error: memory address expected!\n");
			return -1;
		}
		{
			uint8_t read_byte = eeprom_read_byte (mem_addr);
			log_info_P ("read byte @%d: 0x%02x\n", mem_addr, read_byte);
		}
		break;

	case cmd_read_byte_multi:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			log_1_P ("Error: memory address expected!\n");
			return -1;
		}
		{
			eeprom_read_byte_multi (page, 16, mem_addr);
			for (int i = 0; i < 16; i++) {
				log_info_P ("read byte @%d: 0x%02x\n", mem_addr + i, page[i]);
			}
		}
		break;
		
		
	case cmd_write_byte:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			log_1_P ("Error: memory address expected!\n");
			return -1;
		}
		if (!eat_byte_val (&p_input_buf, &byte_val)) {
			log_1_P ("Error: byte val expected!\n");
			return -1;
		}
		eeprom_write_byte (mem_addr, byte_val);
		break;

	case cmd_set_write_enable:
		eeprom_set_write_enable ();
		break;
		

	case cmd_set_write_disable:
		eeprom_set_write_disable ();
		break;

	case cmd_read_temperature:
		{
			uint16_t t = 0;
			mcp9700a_read_temp (&t);
			printf ("temperature: %d/10 C (e.g. 296 => 29,6 C)\n", t);
		}
		break;
		
	case cmd_set_reference_voltage:
		if (!eat_byte_val (&p_input_buf, &byte_val)) {
			log_1_P ("Error: byte val expected!\n");
			return -1;
		}
		reference_voltage = byte_val;
		break;
		
	case cmd_get_reference_voltage:
        printf ("reference voltage: %d (e.g. 33 => 3.3V)\n", reference_voltage);
		break;

	case cmd_set_measurements_start_address:
		{
			msa_t msa_ = 0;
			if (!eat_msa (&p_input_buf, &msa_)) {
				log_1_P ("Error: measurements start address expected!\n");
				return -1;
			}
			if (msa > 1023) {
				log_1_P ("Error: Given Measurments Start Address > 1023\n");
				return -1;
			}
			msa = msa_;
		}		
		break;

	case cmd_get_measurements_start_address:
        printf ("MSA: %d\n", msa);
		break;

	case cmd_set_msat:
		{
			msa_w_time_t msat_ = 0;
			if (!eat_msa (&p_input_buf, &msat_)) {
				log_1_P ("Error: msa with time address expected!\n");
				return -1;
			}
			if (msat > msa_w_time_max || (msat % 6)) {
				log_1_P ("Error: Given msat either too big or not modulo 6!\n");
				return -1;
			}
			msat = msat_;
		}		
		break;		

	case cmd_get_msat:
        printf ("MSAT: %d\n", msat);
		break;

	case cmd_run:
		{
			if (!eat_byte_val(&p_input_buf, &byte_val)) {
				log_1_P ("Error: byte val expected!\n");
				return -1;
			}

			// :TODO:
			log_warn_1_P ("Starting measurements run currently disabled!\n");
			return cmd;
			
			unsigned int duration = 16 * byte_val;
			log_P ("Starting measurements run, duration: %d seconds!\n", duration);

			msa_t msa_ = msa;
			while (byte_val > 0) {
				sleep_mode();
				
				if (got_interrupted) {
					got_interrupted = 0;
					byte_val -= 1;
					{
						uint16_t t = 0;					
						mcp9700a_read_temp (&t);
						mcp9700a_read_temp (&t);
						log_P ("temperature: %d/10 C written to: %u\n", t, msa_);
						msa_ = (msa_ + 1) % (msa_max + 1);
					}
				}
			}
		}
		break;

	case cmd_run_write:
		{
			if (!eat_byte_val(&p_input_buf, &byte_val)) {
				log_1_P ("Error: byte val expected!\n");
				return -1;
			}

			// :TODO:
			log_warn_1_P ("Starting measurements run currently disabled!\n");
			return cmd;
			
			unsigned int duration = 16 * byte_val;
			log_P ("Starting measurements run, duration: %d seconds!\n", duration);
			while (byte_val > 0) {
				sleep_mode();
				
				if (got_interrupted) {
					got_interrupted = 0;
					byte_val -= 1;
					{
						uint16_t t = 0;					
						mcp9700a_read_temp (&t);
						mcp9700a_read_temp (&t);
						eeprom_write_temperature (msa, t);
						log_P ("temperature: %d/10 C written to: %u\n", t, msa);
						msa = (msa + 1) % (msa_max + 1);
					}
				}
			}
		}
		break;

	case cmd_isr_set:
		{
			/* char time_str[] = "00:00:00"; */
			/* time_read_time_string (time_str); */
			/* char date_str[] = "04/02/2018"; */
			/* time_read_date_string (date_str); */
			
			log_1_P ("Starting measurements each 1 minutes.\n");
			activate_timer2();
		}
		break;

	case cmd_isr_unset:
		{
			log_1_P ("Stopping measurements each 1 minutes.\n");
			deactivate_timer2();
		}
		break;
		
	case cmd_get_tmem:
		{
			uint16_t t = 0;
			eeprom_read_temperature (&t, msa);
			log_P ("Read EEPROM temperature: %u @MSA: %u\n", t, msa);
		}
		break;

	case cmd_set_tmem:
		{
			uint16_t t = 0;
			mcp9700a_read_temp (&t);
			mcp9700a_read_temp (&t);
			eeprom_write_temperature (msa, t);
			log_P ("Wrote EEPROM temperature: %u @MSA: %u\n", t, msa);
		}
		break;
	
	case cmd_read_measurement:
		{
			char time_str[9]  = "";
			char date_str[11] = "";
			measurement_t m = { 0 };
			
			eeprom_read_measurement (msat, &m);

			u_time_t t;
			epoch_1970_to_u_time_t (&m.timestamp, &t);

			// :TODO: Generalize as a function in time_lib
			snprintf_P (date_str, 11 * sizeof(char), PSTR("%02u/%02u/%04u")
					  , t.days, t.months, t.years);			
			snprintf_P (time_str, 9 * sizeof(char), PSTR("%02u:%02u:%02u")
					  , t.hours, t.mins, t.secs);

			log_P ("Read measurement from EEPROM address: %u\n"
				   "  time: %s %s, temperature: %u\n"
				   , msat
				   , date_str, time_str
				   , m.temperature
				   );
		}
		break;

	case cmd_write_measurement:
		{
			measurement_t m = { 0 };
			uint32_t now = 0;
			uint16_t t = 0;
			eeprom_mem_address addr = ((eeprom_mem_address) msat) * 6;
			
			mcp9700a_read_temp (&t);
			mcp9700a_read_temp (&t);
			time_to_epoch_1970 (&now);

			m.temperature = t;
			m.timestamp = now;

			eeprom_write_measurement (addr, m);
		}
		break;

	case cmd_time_get:
		/* Get current time and write it to CLI */
		{
			char s[9] = "";
			time_fill_time_string (s);
			log_P ("Current time: %s\n", s);
		}
		break;

	case cmd_date_get:
		/* Get current date and write it to CLI */
		{
			char s[11] = "";
			time_fill_date_string (s);
			log_P ("Current date: %s\n", s);
		}
		break;

	case cmd_time_put:
		/* Set the current time via CLI */
		{
			char s[] = "23:45:00";
			time_read_time_string (p_input_buf);

			time_fill_time_string (s);
			log_P ("Current time: %s\n", s);
		}
		break;

	case cmd_date_put:
		/* Set the current date via CLI */
		{
			char s[] = "04/02/2018";
			time_read_date_string (p_input_buf);

			time_fill_date_string (s);
			log_P ("Current date: %s\n", s);
		}
		break;
		

	}		

	return cmd;
}

/*
  I want to take measurements every 15 minutes (4 times an hour), that
 is each 15 * 60 = 900 seconds.

 Using 8-bit timer2 with divider 1024 using the counter from 1 to 256,
 I can get timer interrupt steps from 31.25ms to 8000ms (8s). Using a
 counter step 192, it allows a non-fraction timing interrupt 192 *
 31.25ms = 6000ms (6s).  Therefore, I need an additional counter with
 up to 150 (150 * 6s = 900s), to get to 900 seconds.
*/
void
activate_timer2()
{
#ifndef EMU	
	// Turn on TIMER2, using a prescaler of 1024. It is expected that
	// a 32765 Hz clock is used, asynchrounsly to the system clock

	// Select external Crystal Oscillator as source (Chapter 18.11.8)
	ASSR |= _BV(AS2);

	// Select 1024 Divider
	TCCR2B |= _BV(CS22) | _BV(CS21) | _BV(CS20); // Write 0b111

	// Use CTC mode
	TCCR2B |= _BV(WGM22);
	TCCR2A |= _BV(WGM21);

	// I'm trying out Compare Mode here, but the intention is
	// that the Interrupt won't change the PIN output behavior
	TIMSK2 |= _BV(OCIE2A);
	
	// Using CLC mode the OCR2A register defines the top/barrier value
	// for timer counter, when it is cleared to zero
	// 192 * 31.25ms = 6000ms = 6s
	OCR2A = 192 - 1;

	// Resetting Timer Counter, to be on the safe side
	TCNT2 = 0;	

	sei();	// Turn interrupts on.
#endif
}

void
deactivate_timer2()
{
#ifndef EMU
	TCCR2B = 0; // Turn off timer2
#endif
}


/* -------------------------------------------------------- */

int
main (void)
{
	unsigned char was_set = 0;

#ifndef EMU
	// Set port C's Pin 
	//DDRC  = 0xFE; // set Port C, all pins as output except pin0 as input
	//PORTC = 0x01; // set all output pins to 0, and set pin0 (input) to pull-up
#endif /* EMU */

#define LINE_LEN 40

	char input_buf[LINE_LEN];
	char* p_buf = input_buf;

#ifndef EMU	
	uart_init();
#endif
	eeprom_init_spi();
	mcp9700a_init();
	/* configure_timer1(); */
	
	for (;;) {
		putchar ('#');
		putchar (' ');

#ifndef EMU		
		if (got_interrupted) {
			got_interrupted = 0;
			putchar ('I');
			putchar ('N');
			putchar ('T');
			putchar ('\n');
			continue;
		}
#endif
 		read_line (input_buf, sizeof (input_buf) - 1, stdin);
		if (strnlen (input_buf, LINE_LEN) == 0)
			continue;
		parse_cmd_line (&p_buf);
	}
	return 0;
}
