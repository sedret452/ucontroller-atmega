/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * 23k256_lib.c: SPI access module for SRAM chip (Microchip 23K256)
 *
 * Copyright	(C) 2013-2022 sedret452
 */

/*
   There are four instructions provided:

   1. Read (0x03)

   2. Write (0x02)

   3. Read status register, RDSR (0x05)

   4. Write status register, WRSR (0x01)

   All instructions have following format:

   1. Byte: Insruction

   2. Byte + 3. Byte: 16-bit memory address (interpreted differently in
   each mode!)


   Within Instructions 3 operation modes can be selected (bits 6 and 7)
   by setting the STATUS register properly

   00 -> byte operation mode (4th byte of operation will be returned value)

   10 (dec 128) -> page operation mode

   - Byte 2 = Page, Byte 3 = Word in page

   - Start to read at given word to end of page
  
   - On roll-over, re-start at beginning of page

   01 (dec 64) -> sequential operation mode

   - Read/Write whole memory, ignore page boundaries (on roll-over)
 */

/*
  TODO

  - "Hold" functionality

  - "Sequential" mode

  - 
 */

/*
  Data is transmitted MSB first, LSB last!
 */
#include "23k256_lib.h"
#include <avr/io.h>
#include "uart_lib.h"
#include "log_lib.h"

const uint8_t  page_size = 32;   // 32 bytes
const uint16_t max_page  = 1023; // 0 to 1023 pages

inline static void set_ss_low  (void) __attribute__((always_inline));
inline static void set_ss_high (void) __attribute__((always_inline));

inline static void
set_ss_low(void)
{
	PORTB &= ~(1<<PORTB2);
	/* PORTB &= ~(_BV(PB2)); */
}

inline static void
set_ss_high(void)
{
	PORTB |=  (1<<PORTB2);
	/* PORTB |= _BV(PB2); */
}

static uint8_t spi_transfer (uint8_t data);

void
sram_init_spi ()
{
	// Configure MOSI, SCK and SS for output
	DDRB = (1 << PB3) | (1 << PB5) | (1 << PB2); // MOSI, SCK, SS

	// Enable SPI, as master and as SPI mode 0 (CPOL = 0, CPHA = 0)
	SPCR = (1 << SPE) | (1 << MSTR);// | (1 << CPOL) | (1 << CPHA);

	uint8_t __attribute__ ((unused)) clear = 0;
	clear = SPSR;
	clear = SPDR;
}

static uint8_t
spi_transfer (uint8_t data)
{
	log_debug ("spi_transfer(): data: 0x%x\n", data);
	
	SPDR = data;
	loop_until_bit_is_set (SPSR, SPIF);
	return SPDR;
}

sram_op_mode
sram_read_status_register ()
{
	sram_op_mode mode = RESERVED_MODE;
	uint8_t _mode = 0;

	set_ss_low();
	spi_transfer (RDSR);
	_mode = spi_transfer (0xFF);
	set_ss_high();
	
	log_debug ("sram read status: 0x%x\n", _mode);
	mode = _mode;
	
	return mode; // TODO
}

int
sram_write_status_register (sram_op_mode mode)
{
	log_debug ("trying to write following mode: 0x%x\n", mode);

	set_ss_low();
	spi_transfer (WRSR);
	spi_transfer (mode);
	set_ss_high();

	return mode; // TODO
}

uint8_t
sram_read_byte (sram_mem_address_t address)
{
	unsigned char _val = 0;

	set_ss_low();
	spi_transfer (READ);
	spi_transfer ( (unsigned char) (address >> 8));
	spi_transfer (address);
	_val = spi_transfer (0xff);
	set_ss_high();

	log_debug ("sram_read_byte(): _val: 0x%x\n", _val);

	return -1; // TODO
}

int
sram_write_byte (sram_mem_address_t address, uint8_t val)
{
	set_ss_low();
	spi_transfer (WRITE);
	spi_transfer (address >> 8);
	spi_transfer (address);
	spi_transfer (val);
	set_ss_high();
	
	return -1;
}

int
sram_read_page (uint8_t* val_array, uint8_t page, uint8_t addr_in_page)
{
	uint8_t start = addr_in_page;
	uint8_t i = start;
	log_debug ("sram_read_page(): page: 0x%x, address: 0x%x\n", page, start);
	
	set_ss_low();
	spi_transfer (READ);
	spi_transfer (page);
	spi_transfer (start);
	
	for (; i < page_size; i++) {
		val_array[i] = spi_transfer (0xff);
	}
	set_ss_high();
}

int
sram_write_page (uint8_t page, uint8_t* val_array, uint8_t addr_in_page)
{
	uint8_t start = addr_in_page;
	uint8_t i = start;
	log_debug ("sram_write_page(): page: 0x%x, address: 0x%x\n", page, start);
	
	set_ss_low();
	spi_transfer (WRITE);
	spi_transfer (page);
	spi_transfer (start);
		
	for (; i < page_size; i++) {
		spi_transfer (val_array[i]);
	}
	set_ss_high();
}

int
sram_read_sequential (uint8_t* val_vec, uint16_t len, uint8_t page, uint8_t addr_in_page)
{
	log_debug ("sram_read_squential(): starting at page %d - addr: %d, len: %d\n"
			   , page, addr_in_page, len);

	set_ss_low();
	spi_transfer (READ);
	spi_transfer (page);
	spi_transfer (addr_in_page);

	for (int i = 0; i < len; i++) {
		val_vec[i] = spi_transfer (0xff);
	}
	set_ss_high();
}

int
sram_write_sequential (uint8_t page, uint8_t addr_in_page, uint8_t* val_vec, uint16_t len)
{
	log_debug ("sram_write_squential(): starting at page %d - addr: %d, len: %d\n"
			   , page, addr_in_page, len);

	set_ss_low();
	spi_transfer (WRITE);
	spi_transfer (page);
	spi_transfer (addr_in_page);

	for (int i = 0; i < len; i++) {
		spi_transfer (val_vec[i]);
	}
	set_ss_high();
}
