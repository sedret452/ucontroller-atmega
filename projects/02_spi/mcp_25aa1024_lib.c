/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * mcp_25aa1024_lib.c: SPI access module for SRAM chip (Microchip 25aa1024)
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include "mcp_25aa1024_lib.h"
#include <avr/io.h>
#include "uart_lib.h"
#include "log_lib.h"

#include <avr/pgmspace.h>

/*
  Basic Circuit :TODO:

 */

/*
  Data is transmitted MSB first, LSB last!
 */

#define MEM_SIZE 0x20000 // ((1024 * 1024) / 8)  bytes

const uint16_t page_size = 256;   // 256 bytes
const uint16_t max_page  = 511;  // 0 to 511 pages

inline static void set_ss_low  (void) __attribute__((always_inline));
inline static void set_ss_high (void) __attribute__((always_inline));

inline static void
set_ss_low(void)
{
	PORTB &= ~(1<<PORTB2);
}

inline static void
set_ss_high(void)
{
	PORTB |=  (1<<PORTB2);
}

// :TODO: This creates a calling overhead.  I think it's fine for
// current prototypical development approach.  Once, the code has
// reached functional stability, the approach should change to
// inlining (excluding the usage of log_P)!
static uint8_t
spi_transfer (uint8_t data)
{
	log_P ("spi_transfer(): data: 0x%x\n", data);
	
	SPDR = data;
	loop_until_bit_is_set (SPSR, SPIF);
	return SPDR;
}

void
eeprom_init_spi ()
{
	// Configure MOSI, SCK and SS for output
	DDRB = (1 << PB3) | (1 << PB5) | (1 << PB2); // MOSI, SCK, SS

	// Enable SPI, as master and as SPI mode 0 (CPOL = 0, CPHA = 0)
	SPCR = (1 << SPE) | (1 << MSTR);// | (1 << CPOL) | (1 << CPHA);

	uint8_t __attribute__ ((unused)) clear = 0;
	clear = SPSR;
	clear = SPDR;
}

eeprom_status
eeprom_read_status ()
{
	eeprom_status status = 0x0;
	set_ss_low();

	spi_transfer (RDSR);
	// SPI works as a shift register, therefore we need to transfer
	// data to the slave device, in order to get data from it
	status = spi_transfer (0xff);
	set_ss_high();

	log_P ("eeprom_read_status(): status: 0x%x\n", status);

	return status;
}

void
eeprom_write_status (eeprom_status status)
{
	set_ss_low();

	spi_transfer (WRSR);
	// SPI works as a shift register, therefore we need to transfer
	// data to the slave device, in order to get data from it
	spi_transfer (status);
	set_ss_high();

	log_P ("eeprom_write_status(): status: 0x%x written\n", status);
}

uint8_t
eeprom_read_byte (eeprom_mem_address address)
{
	uint8_t res = 0;

	set_ss_low();

	// TODO
	// is this the right order regarding MSB?
	spi_transfer (READ);
	spi_transfer (address.byte_hi);
	spi_transfer (address.byte_mid);
	spi_transfer (address.byte_low);

	// SPI works as a shift register, therefore we need to transfer
	// data to the slave device, in order to get data from it
	res = spi_transfer (0xff);
	set_ss_high();

	log_P ("eeprom_read_byte(): res: 0x%x\n", res);

	return res;
}

/*
  Note, the maximum size of consecutive read operations is 256!  Since
  ATmega328p's RAM is pretty much limited anyway, this is fine.
*/
void
eeprom_read_byte_multi (uint8_t* buf, uint8_t length, eeprom_mem_address address)
{
	// :TODO: proper 24-bit address handling is neede here!
	// Calculations should be based on macros!
	/* if (address >= MEM_SIZE) { */
	/* 	log_P ("Error: Address too big: 0x%04x >= 0x%04x\n" */
	/* 		 , address, MEM_SIZE); */
	/* 	return; */
	/* } */

	// TODO
	// is this the right order regarding MSB?
	spi_transfer (READ);
	spi_transfer (address.byte_hi);
	spi_transfer (address.byte_mid);
	spi_transfer (address.byte_low);
    for (int i = 0; i < length; i++) {
		buf[i] = spi_transfer (0xff);
    }
	set_ss_high();
}


void
eeprom_write_byte (eeprom_mem_address address, uint8_t val)
{
	set_ss_low();

	// TODO
	// is this the right order regarding MSB?
	spi_transfer (WRITE);
	spi_transfer (address.byte_hi);
	spi_transfer (address.byte_mid);
	spi_transfer (address.byte_low);
	spi_transfer (val);

	set_ss_high();

	log_P ("eeprom_write_byte(): val: 0x%x\n", val);
}

/* Note, section 2.3 of the datasheet lists all conditions for the
   write disable to be resetted.
*/
void
eeprom_set_write_enable (void)
{
	set_ss_low();
	spi_transfer (WREN);
	set_ss_high();
	/* log_P ("eeprom_set_write_enable(): Write enabled.\n"); */
}

void
eeprom_set_write_disable (void)
{
	set_ss_low();
	spi_transfer (WRDI);
	set_ss_high();
	/* log_P ("eeprom_set_write_disable(): Write disabled.\n"); */
}
