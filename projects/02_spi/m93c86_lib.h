/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * m93c86_lib.h: SPI access module for EEPROM chip (STM m93c86)
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#ifndef __M93C86_LIB_H_
#define __M93C86_LIB_H_

#if (MCU != atmega328p)
 #error "m93c86_lib is only usable for ATmega328p!  Be sure to set properly MCU variable."
#endif

#include <inttypes.h>

/*
  Instruction are split in (i) 2 Bit opcode and (ii) 11 Bit Address.
  Furthermore all instructions using 0x00 as opcode will use first two
  Bits of address to code the instructions!

  Be aware: This is valid only for Byte-operation, not for Word
  (16bit).

  See table 6 in the datasheet.
*/

/* :TODO: Why does the instruction type need to be in the (public)
 *        header?  Shouldn't this be in the module only?
 */
enum _m93c86_instruction {
	READ  = 0x02, // opcode b10, no address bits
	WRITE = 0x01, // opcode b01, no address bits
	/* WEN, */    // opcode b00, first two address bits 11
	/* WDS, */    // opcode b00, first two address bits 00
	ERASE = 0x03//,  opcode b11, no address bits
	/* ERAL  = 0x00, */ // opcode b00, first two address bits 10
	/* WRAL */ // opcode b00, first two address bits 01
};
typedef enum _m93c86_instructions eeprom_instruction;
typedef uint16_t eeprom_mem_address;

// Maximum address
#define eeprom_mem_address_max (2048 - 1)

inline const uint16_t
eeprom_mem_address_to_num (eeprom_mem_address addr) __attribute__((always_inline));

inline const uint16_t
eeprom_mem_address_to_num (eeprom_mem_address addr)
{
	return addr;
}


void eeprom_init_spi ();
uint8_t eeprom_read_byte (eeprom_mem_address address);
void eeprom_read_byte_multi (uint8_t* page, uint8_t length, eeprom_mem_address address);
void eeprom_write_byte (eeprom_mem_address address, uint8_t val);
void eeprom_busy_wait (void);
void eeprom_set_write_enable (void);
void eeprom_set_write_disable (void);

#endif /* __M93C86_LIB_H_ */
