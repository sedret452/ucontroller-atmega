/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * mcp_25aa1024_lib.h: SPI access module for SRAM chip (Microchip 25aa1024)
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#ifndef __MCP_25AA1024_LIB_H_
#define __MCP_25AA1024_LIB_H_

#if (MCU != atmega328p)
 #error "mcp_25aa1024_lib is only usable for ATmega328p!  Be sure to set properly MCU variable."
#endif /* (MCU != atmega328p) */

#include <inttypes.h>

/* 
  An 8-bit instruction register is used.

  The address space is 24-bit wide (3x8 bit).

  Page size of 256 bytes => 512 pages

  Sector handling for protection ??
  - ? Which sectors are there?

  :TODO: Deep Power Down Mode!!!
*/

enum _mcp_25aa1024_instruction {
	// Read byte
	READ  = 0x03, // opcode b011
	// Write byte
	WRITE = 0x02, // opcode b010
	// Write enable
	WREN = 0x06, //  opcode b110
	// Write disable
	WRDI = 0x08, //  opcode b100
	// Read Status Register
	RDSR = 0x08, //  opcode b101
	// Write Status Register
	WRSR = 0x01, //  opcode b001
	/* PE   = 0x62, */   // opcode b0100,0010
	/* SE   = 0xD8, */   // opcode b1101,1000
	/* CE   = 0xC7, */   // opcode b1100,0111
	/* RDID = 0xAB, */   // opcode b1010,1011
	/* DPD  = 0xB9  */   // opcode b1011,1001
};
typedef enum _mcp_25aa1024_instruction eeprom_instruction;

// TODO: Should I put someting like "packed" here?
typedef struct _eeprom_mem_address {
	uint8_t byte_hi;
	uint8_t byte_mid;
	uint8_t byte_low;
} eeprom_mem_address;

// Maximum address
#define eeprom_mem_address_max  (131072 - 1)

inline const uint32_t
eeprom_mem_address_to_num (eeprom_mem_address addr) __attribute__((always_inline));

inline const uint32_t
eeprom_mem_address_to_num (eeprom_mem_address addr)
{
	return ((uint32_t) addr.byte_hi << 16) + ((uint32_t) addr.byte_mid << 8) + (uint32_t) addr.byte_low;
}

typedef uint8_t eeprom_status;

#define eeprom_status_wip  (1<<0);
#define eeprom_status_wel  (1<<1);
#define eeprom_status_bp0  (1<<2);
#define eeprom_status_bp1  (1<<3);
#define eeprom_status_wpen (1<<7);

void eeprom_init_spi ();
uint8_t eeprom_read_byte (eeprom_mem_address address);
void eeprom_read_byte_multi (uint8_t* buf, uint8_t length, eeprom_mem_address address);
eeprom_status eeprom_read_status ();
void eeprom_write_status (eeprom_status status);
void eeprom_write_byte (eeprom_mem_address address, uint8_t val);
void eeprom_set_write_enable (void);
void eeprom_set_write_disable (void);

#endif /* __MCP_25AA1024_LIB_H_ */
