/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * m93c86_lib.c: SPI access module for EEPROM chip (STM m93c86)
 *
 * Copyright	(C) 2013-2023 sedret452
 */

#include "m93c86_lib.h"
#include "uart_lib.h"
#include "log_lib.h"
#include <avr/pgmspace.h>

/*
  TODO:

    - Complete functionality: ERASE, ERAL, WRAL

	- Prepare for 8-bit and 16-bit read/write support

	- Externalize Soft-SPI part into a library by it's own

	- Make PIN selection configurable, in a way, that this library
      could be called multiple times
 */


#define MEM_SIZE_ORG_8   (1<<11)
#define MEM_SIZE_ORG_16  (1<<10)
#define MEM_SIZE         MEM_SIZE_ORG_8

#include "m93c86_pin.h"

inline static void spi_soft_ss_low (void) __attribute__((always_inline));
inline static void spi_soft_ss_high (void) __attribute__((always_inline));
inline static void spi_soft_sck_low (void) __attribute__((always_inline));
inline static void spi_soft_sck_high (void) __attribute__((always_inline));
inline static void spi_soft_mosi_low (void) __attribute__((always_inline));
inline static void spi_soft_mosi_high (void) __attribute__((always_inline));

inline static void
spi_soft_ss_low(void)
{
	SPI_SOFT_PORT &= ~(1<<SPI_SOFT_PIN_SS);
}

inline static void
spi_soft_ss_high(void)
{
	SPI_SOFT_PORT |= (1<<SPI_SOFT_PIN_SS);
}

inline static void
spi_soft_sck_high (void)
{
	SPI_SOFT_PORT |= (1<<SPI_SOFT_PIN_SCK);
}

inline static void
spi_soft_sck_low (void)
{
	SPI_SOFT_PORT &= ~(1<<SPI_SOFT_PIN_SCK);
}

inline static void
spi_soft_mosi_high (void)
{
	SPI_SOFT_PORT |= (1<<SPI_SOFT_PIN_MOSI);
}

inline static void
spi_soft_mosi_low (void)
{
	SPI_SOFT_PORT &= ~(1<<SPI_SOFT_PIN_MOSI);
}

static uint16_t
spi_soft_recv (uint8_t nbits)
{
	uint16_t val = 0;
	
	for (uint8_t i = 1; i <= nbits; i++) {
		val = val << 1;
		spi_soft_sck_high ();
		if (bit_is_set (SPI_SOFT_IN_PIN, SPI_SOFT_PIN_MISO)) {
			val++;
		}
#ifndef EMU
		__builtin_avr_delay_cycles (8);
#endif
		spi_soft_sck_low ();
	}
	log_debug_P ("spi_soft_recv(): received 0x%04x\n", val);
	return val;
}

void
spi_soft_send (uint8_t nbits, uint16_t val)
{
	log_debug_P ("spi_soft_send(): before corection 0x%04x\n", val);
	val <<= (16 - nbits); // adjust val to MSB
	log_debug_P ("spi_soft_send(): after corection 0x%04x\n", val);
	
	for (uint8_t i = 1; i <= nbits; i++) {
		if (val & 0x8000) {
			spi_soft_mosi_high ();
		}
		else {
			spi_soft_mosi_low ();
		}
		spi_soft_sck_high ();
#ifndef EMU
		__builtin_avr_delay_cycles (8);
#endif
		spi_soft_sck_low ();
		val <<= 1;
	}
}

void
eeprom_init_spi ()
{
	// Configure MOSI, SCK and SS for output
	SPI_SOFT_DDR = (1 << SPI_SOFT_PIN_MOSI) | (1 << SPI_SOFT_PIN_SCK) | (1 << SPI_SOFT_PIN_SS);
}

/* For the m93c86 in 8-bit mode (byte mode) is of maximum size 11 bit.
   In the 16-bit mode (word mode) it is of maximum size 10 bit.
 */
uint8_t
eeprom_read_byte (eeprom_mem_address address)
{
	uint8_t res = 0;
	
	if (address >= MEM_SIZE) {
		log_P ("Error: Address too big: 0x%04x >= 0x%04x\n"
			 , address, MEM_SIZE);
		return res;
	}
	
	// For read 14 bits are used:
	//  "1" start bit + "10" opcode + 11 bits
	uint16_t _cmd = (1 << 13) | (READ << 11) | address;

	spi_soft_ss_high();
	spi_soft_send (14, _cmd);
	res = spi_soft_recv (8);
	spi_soft_ss_low();

	return res;
}

void
eeprom_read_byte_multi (uint8_t* page, uint8_t length, eeprom_mem_address address)
{
	if (address >= MEM_SIZE) {
		log_P ("Error: Address too big: 0x%04x >= 0x%04x\n"
			 , address, MEM_SIZE);
		return;
	}
	
	// For read 14 bits are used:
	//  "1" start bit + "10" opcode + 11 bits
	uint16_t _cmd = (1 << 13) | (READ << 11) | address;

	spi_soft_ss_high();
	spi_soft_send (14, _cmd);
    for (int i = 0; i < length; i++) {
		page[i] = spi_soft_recv (8);
    }
	spi_soft_ss_low();

	return;
}


void
eeprom_write_byte (eeprom_mem_address address, uint8_t val)
{
	// TODO: Check if address is bigger than 11 bits
	if (address > (1 << 12)) {
		log_P ("Error: Address too big: 0x%04x > 0x%04x\n"
			 , address, (1 << 12));
		return;
	}
   
	uint16_t _cmd = (1 << 13) | (WRITE << 11) | address;

	spi_soft_ss_high ();
	spi_soft_send (14, _cmd);
	spi_soft_send (8, val);
	spi_soft_ss_low ();

	spi_soft_ss_high ();
	eeprom_busy_wait ();
	spi_soft_ss_low  ();
}

void
eeprom_busy_wait (void)
{
	// TODO: Try something like interrupt handler instead of busy wait
	while (bit_is_clear (SPI_SOFT_IN_PIN, SPI_SOFT_PIN_MISO))
		;
	log_debug_1_P ("eeprom_busy_wait(): ready now!\n");
}

void
eeprom_set_write_enable (void)
{
	uint16_t _cmd = (1 << 13) | (3 << 9);

	spi_soft_ss_high ();
	spi_soft_send (14, _cmd);
	spi_soft_ss_low  ();
}

void
eeprom_set_write_disable (void)
{
	uint16_t _cmd = (1 << 13);

	spi_soft_ss_high ();
	spi_soft_send (14, _cmd);
	spi_soft_ss_low ();
}
