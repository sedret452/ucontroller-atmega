/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * m93c86_pin.h: EEPROM chip (STM m93c86), pin/port config.
 *
 * Copyright	(C) 2013-2023 sedret452
 */

#ifndef __M93C86_PIN_H_
#define __M93C86_PIN_H_

/*
  Remember: Since m93c86 is Three-wire (3WI) chip -- instead of SPI --, Chip
  Select (CS) "high" will select the chip!

  Basic circuit 
  
  S = Chip Selelct
  D = Serial Data Input 
  Q = Serial Data Output
  C = Clock
  ORG = Organization Select
  V_CC = Supply voltag
  V_SS = Ground

                   GND
                    |
                    R (100 kOhm)
                    |
  PB5 (CSK) <-------+---> (C) PIN2
  PB4 (MISO) <----------> (Q) PIN4
  PB3 (MOSI) <----------> (D) PIN3
  PB2 (SS) <----+-------> (S) PIN1
                |         (V_CC) PIN8 <---> VCC (3.3V)
	 (100 kOhm) R         (V_SS) PIN5 <-+-> GND
				|                       |
			   GND        (ORG)  PIN6 <-+

 Remember, all soft SPI pin configuration *must* be consistent with soft SPI
 *Port* configuration. That is, if you configure:

  'SPI_SOFT_PORT' -> 'PORTC'
   
 and want to use:

  'SPI_SOFT_PIN_MOSI' -> 'PB3'

 that *won't* work.
*/

/* Include I/O address names */
#include <avr/io.h>

/* Overall used port, e.g. PORTB */
#define SPI_SOFT_PORT    PORTB

/* Used data direction register, e.g. DDRB. */
#define SPI_SOFT_DDR     DDRB

/* General *input* pin address, e.g. PINB. */
#define SPI_SOFT_IN_PIN  PINB

/* These are the configuration for the actual used pins. Please keep
 * in mind MISO has to be an input pin. */
#define SPI_SOFT_PIN_SCK   PB5
#define SPI_SOFT_PIN_MISO  PINB4
#define SPI_SOFT_PIN_MOSI  PB3
#define SPI_SOFT_PIN_SS    PB2

#endif /* __M93C86_PIN_H_ */
