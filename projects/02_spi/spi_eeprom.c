/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * spi_eeprom.c: UART-based CLI for EEPROM (SPI) access.
 *
 *   A CLI is provided via UART and allows to manipulate a connected
 *   EEPROM chip (connected via software/hardware SPI). It allows to
 *   initialize the chip, read/write bytes or pages, or even a
 *   sequence of bytes.
 *
 *   Note: The CLI is provided with online help.  Of course, this
 *   blows up memory usage heavily, due to the amount of stored text.
 *   You've been warned.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

/*

  TODO
  ----

  - implement "sget" and "sput" using symbolic mode values

  - Should mode selection be internal in pget and pput?

  - Proper backspace behavior (backspace *and delete*!)

  - Ctrl-l will cause clear, but the prompt needs to be redrawn!

  - Re-introduce "-Os" optimization, check compiled Bytecode

  - New CLI command structure:

      init

	  help

	  get byte <address>
	  put byte <address> <value>

	  get page <page> [address in page]
	  put page <page> [address in page]

	  get stat
	  put stat <status>

	  get seq <page> <address in page> <len>
	  put seq <page> <address in page> <len>

  - Fix CLI help command (there's an error at runtime too!).
  
  - Fix log_lib.h so that each time the log level is adjusted *all*
    dependend targets are adjusted correctly.  This seems not to work properly.

  - log_lib.h: How to merge log_debug_1() macro with log_debug() for
    all similar macros.

  - Implement m93c86 correctly (use soft spi for that).

    How to properly fit m93c86 into current CLI structure?

  - Find a proper way to fit *all* USART messages into __FLASH instead
    into stack!
   
*/

#include <stdlib.h>
#include <stdio.h>

#include <string.h> // strcmp()
#include <inttypes.h>

#ifndef EMU
 #include <avr/io.h>
 #include "uart_lib.h"

 #define M93C86       1
 #define MCP_25AA1024 2

 #ifndef EEPROM_MODEL
  #define EEPROM_MODEL M93C86
 #endif /* EEPROM_MODEL */

 #if EEPROM_MODEL == M93C86
  #include "m93c86_lib.h"
 #elif EEPROM_MODEL == MCP_25AA1024
  #include "mcp_25aa1024_lib.h"
  /* #error "EEPROM_MODEL 'MCP_25AA1024' not supported!" */
 #else
  #error "EEPROM_MODEL unknown!"
 #endif /* EEPROM_MODEL == ... */

#endif /* EMU */

#include "log_lib.h"

#include <ctype.h> /* for iscntrl() */
void read_line (char* input_buf, int max_len, FILE* stream);

enum _cmd_t {
	cmd_read_byte = 0
	, cmd_write_byte = 1
	, cmd_init = 2
	, cmd_set_write_enable = 3
	, cmd_set_write_disable = 4
	, cmd_help = 5
	, cmd_error = 6
	, cmd_read_byte_multi = 7
#if EEPROM_MODEL == MCP_25AA1024
	, cmd_get_status = 8
	, cmd_put_status = 9
#endif /* EEPROM_MODEL == MCP_25AA1024 */
	
};
typedef enum _cmd_t cmd_t;

#define ONLINE_HELP
#ifdef ONLINE_HELP


#if defined __FLASH

#include <avr/pgmspace.h>

static const __flash char cmd_init_usage [] =
	"init - (re-)initialize SPI\n"
	"usage: init\n";
static const char __flash cmd_read_byte_usage[] =
	"get byte - read from memory address (byte mode)\n"
	"usage: get byte <address>\n";
static const __flash char cmd_write_byte_usage[] =
	"put byte - write to memory address (byte mode)\n"
	"usage: put byte <address> <value>\n";
static const __flash char cmd_set_write_enable_usage[] =
	"put wenb - set write enable\n"
	"usage: put wenb\n";
static const __flash char cmd_set_write_disable_usage[] =
	"put wdis - set write disable\n"
	"usage: put wdis\n";
#if EEPROM_MODEL == MCP_25AA1024
static __flash const char cmd_get_status_usage[] =
	"get stat - read eeprom status\n"
	"usage: get stat\n";
static __flash const char cmd_put_status_usage[] =
	"put stat - put eeprom status\n"
	"usage: put stat <mode>\n"
	"  ??\n";
#endif /* EEPROM_MODEL == MCP_25AA1024 */

static const char __flash cmd_read_byte_multi_usage[] =
	"get mult - read multiples from memory address (byte mode), page\n"
	"usage: get mult <address>\n";


static __flash const char* usage_vec[] = {
	cmd_read_byte_usage
	, cmd_write_byte_usage
	, cmd_init_usage
	, cmd_set_write_enable_usage
	, cmd_set_write_disable_usage
	, cmd_read_byte_multi_usage
	
#if EEPROM_MODEL == MCP_25AA1024
	, cmd_get_status_usage
	, cmd_put_status_usage
#endif /* EEPROM_MODEL == MCP_25AA1024 */	
};

#elif defined EMU

 #error "No emulation supported for m93c86 library"

};
#else

 #error "No flash namespace available"

#endif /* FLASH , EMU */
#endif /* ONLINE_HELP */

#ifdef ONLINE_HELP

int
print_usage (cmd_t cmd)
{
	if (cmd == cmd_error) {
#ifdef __FLASH

		// I know, %S has usually a different meaning, so lets turn
		// off type warnings here
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
		printf ("%S%S%S%S%S%S"
#if EEPROM_MODEL == MCP_25AA1024
				"%S%S"
#endif /* EEPROM_MODEL == MCP_25AA1024 */	
				, cmd_init_usage
				, cmd_read_byte_usage
				, cmd_write_byte_usage
				, cmd_set_write_enable_usage
				, cmd_set_write_disable_usage
				, cmd_read_byte_multi_usage
#if EEPROM_MODEL == MCP_25AA1024
				, cmd_get_status_usage
				, cmd_put_status_usage
#endif /* EEPROM_MODEL == MCP_25AA1024 */	
				);
#pragma GCC diagnostic pop		
		
#else /* not __FLASH */
		printf ("%s%s%s%s%s%s"
#if EEPROM_MODEL == MCP_25AA1024
				"%S%S"
#endif /* EEPROM_MODEL == MCP_25AA1024 */	
				, cmd_init_usage
				, cmd_get_status_usage
				, cmd_put_status_usage
				, cmd_read_byte_usage
				, cmd_write_byte_usage
				, cmd_read_page_usage
				, cmd_write_page_usage
#if EEPROM_MODEL == MCP_25AA1024
				, cmd_get_status_usage
				, cmd_put_status_usage
#endif /* EEPROM_MODEL == MCP_25AA1024 */	
				);
#endif /* __FLASH */
	}
	else {
#ifdef __FLASH
		printf ("%S\n", usage_vec[cmd]);
#else
		printf ("%s", (usage_vec[cmd]));
#endif
	}
	return 0;
}

#endif /* ONLINE_HELP */

void
read_line (char* input_buf, int cur_len, FILE* stream)
{
	int c = EOF;
	char* p = input_buf;

	*p = '\0'; // safe initialization
	
	if (cur_len < 2)
		return;
	
	for (;;) {
		c = getc (stream);
		
		switch (c) {
			
		case EOF:
			*p = '\0';
			return;
			
		// TODO:
		//  - 0x04 (end of transmission -- (execute exit)
		//  - Use '\f' analog to '\n'
		case '\n':
		case '\r':
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
			
		case '\f': // form feed
			putchar ('\f');
			break;
			
		case '\b': // back space
			if ( (p - input_buf) != 0 ) {
				p--;
				cur_len++;
				putchar ('\b');
				putchar (' ');
				putchar ('\b');
			}
			break;
			
		default:
			if (iscntrl (c)) {
				// DEBUG output
				log_warn_P ("Error: unknown control sequence: %x", c);
			}
			else {// echo character and write into buffer
				*p = (char) c;
				p++;
				cur_len--;
				putchar (c);
			}
		}
		
		if (cur_len <= 1) {
			putchar ('\r');
			putchar ('\n');
			*p = '\0';
			return;
		}
	}	
	return;
}

int
eat_byte_val (char** input, uint8_t* byte_val)
{
	int n = 0;
	/* int eat = sscanf (*input, "%u"SCNu8, *byte_val); */
	int eat = sscanf_P (*input, PSTR("%hhd%n"), byte_val, &n);
	
	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}

#if EEPROM_MODEL == MCP_25AA1024
int
eat_mem_address (char** input, eeprom_mem_address* addr)
{
	int n = 0;
	uint32_t t = 0;
	/* int eat = sscanf (*input, "u%"SCNu32"%n", addr, &n); */
	int eat = sscanf_P (*input, PSTR("%ld%n"), &t, &n);

	addr->byte_low = t & 0xFF;
	addr->byte_mid = (t & 0xFF00) >> 8;
	addr->byte_hi  = (t & 0xFF0000) >> 16;

	log_debug_P ("read mem address: %02x (hi) %02x (mid) %02x (lo)\n"
				 , addr->byte_hi
				 , addr->byte_mid
				 , addr->byte_lo
				 );

	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}
#else
int
eat_mem_address (char** input, eeprom_mem_address* addr)
{
	int n = 0;
	/* int eat = sscanf (*input, "u%"SCNu16"%n", addr, &n); */
	int eat = sscanf_P (*input, PSTR("%hd%n"), addr, &n);

	if (EOF == eat)
		return 0;
	else {
		*input += n;
		return 1;
	}
}
#endif /* EEPROM_MODEL == MCP_25AA1024 */	



int
eat_cmd (char** input)
{
	char _cmd_1[4 + 1] = ""; // cmd first part get, put, init, help (length 4 + 1)
	char _cmd_2[4 + 1] = ""; // cmd second part (max length 4 + 1)
	enum _cmd_class { NONE, GET, PUT };
	typedef enum _cmd_class cmd_class;
	cmd_class class = NONE;

	int n = 0;
	sscanf_P (*input, PSTR("%s%n"), _cmd_1, &n);

	if (!strncmp_P (_cmd_1, PSTR("init"), 4)) {
		*input += n;
		return cmd_init;
	}
	else if (!strncmp_P (_cmd_1, PSTR("get"), 3)) {
		class = GET;
		*input += n;
	}
	else if (!strncmp_P (_cmd_1, PSTR("put"), 3)) {
		class = PUT;
		*input += n;
	}
#ifdef ONLINE_HELP
	else if (!strncmp_P (_cmd_1, PSTR("help"), 4)) {
		*input += n;
		return cmd_help;
	}
#endif /* ONLINE_HELP */

	else {
		// error
		return cmd_error;
	}

	sscanf_P (*input, PSTR("%s%n"), _cmd_2, &n);
	if (!strncmp_P (_cmd_2, PSTR("byte"), 4)) {
		*input +=n;
		return (class == GET) ? cmd_read_byte : cmd_write_byte;
	}
	else if (!strncmp_P (_cmd_2, PSTR("wenb"), 4)) {
		*input +=n;
		return (class == PUT) ? cmd_set_write_enable : cmd_error;
	}
	else if (!strncmp_P (_cmd_2, PSTR("wdis"), 4)) {
		*input +=n;
		return (class == GET) ? cmd_set_write_disable : cmd_error;
	}
	else if (!strncmp_P (_cmd_2, PSTR("mult"), 4)) {
		*input +=n;
		return (class == GET) ? cmd_read_byte_multi : cmd_error;
	}
#if EEPROM_MODEL == MCP_25AA1024
	else if (!strncmp_P (_cmd_2, PSTR("stat"), 4)) {
		*input +=n;
		return (class == GET) ? cmd_get_status : cmd_put_status;
	}
#endif /* EEPROM_MODEL == MCP_25AA1024 */
	else {
		return cmd_error;
	}
}

int
parse_cmd_line (char** input_buf)
{
	char* p_input_buf = *input_buf;
	cmd_t cmd = cmd_error;
	eeprom_mem_address    mem_addr = { 0 };
	// TODO
	/* eeprom_mem_address* p_mem_addr = &mem_addr; */
	uint8_t    byte_val = 0;
	// TODO
	/* uint8_t* p_byte_val = &byte_val; */
	uint8_t page[16];

	log_debug_P ("before *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);
	cmd = eat_cmd (&p_input_buf);
	log_debug_P ("after *p_input_buf(0x@%x): >%c<\n", p_input_buf, *p_input_buf);

	if (cmd == cmd_error) {
		log_P ("Error: Wrong cmd:\n  %s\n", p_input_buf);
		return -1;
	}

	switch (cmd) {
#ifdef ONLINE_HELP
	case cmd_help:
		{
			cmd_t help_for_cmd = eat_cmd (&p_input_buf);
			print_usage (help_for_cmd);
		}
		break;
#endif /* ONLINE_HELP */
		
	case cmd_init:
		log_info_1_P ("Initialization\n");
		eeprom_init_spi();
		break;
		
	case cmd_read_byte:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			log_1_P ("Error: memory address expected!\n");
			return -1;
		}
		{
			uint8_t read_byte = eeprom_read_byte (mem_addr);
			log_info_P ("read byte @%d: 0x%02x\n", mem_addr, read_byte);
		}
		break;

	case cmd_read_byte_multi:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			log_1_P ("Error: memory address expected!\n");
			return -1;
		}
		{
			eeprom_read_byte_multi (page, 16, mem_addr);
			for (int i = 0; i < 16; i++) {
				log_info_P ("read byte @%d: 0x%02x\n"
							, (eeprom_mem_address_to_num(mem_addr) + i) % (eeprom_mem_address_max + 1)
							, page[i]);
			}
		}
		break;
		
		
	case cmd_write_byte:
		if (!eat_mem_address (&p_input_buf, &mem_addr)) {
			log_1_P ("Error: memory address expected!\n");
			return -1;
		}
		if (!eat_byte_val (&p_input_buf, &byte_val)) {
			log_1_P ("Error: byte val expected!\n");
			return -1;
		}
		eeprom_write_byte (mem_addr, byte_val);
		break;

	case cmd_set_write_enable:
		eeprom_set_write_enable ();
		break;

	case cmd_set_write_disable:
		eeprom_set_write_disable ();
		break;

#if EEPROM_MODEL == MCP_25AA1024
	case cmd_get_status:
		{
			eeprom_status stat = eeprom_read_status();
			// :TODO: Provide better string representation of set
			// status bits
			log_info_P ("Status: 0x%02x\n", stat);
		}
		break;

	case cmd_put_status:
		if (!eat_byte_val (&p_input_buf, &byte_val)) {
			log_1_P ("Error: byte val expected!\n");
			return -1;
		}
		log_debug_P ("Setting status: 0x%02x IGNORED\n", byte_val);
		// :TODO: unignore writing status
		//eeprom_write_status (byte_val);
		break;
#endif /* EEPROM_MODEL == MCP_25AA1024 */
		

	}
	return cmd;
}

/* -------------------------------------------------------- */

int
main (void)
{
	unsigned char was_set = 0;

#ifndef EMU
	// Set port C's Pin 
	DDRC  = 0xFE; // set Port C, all pins as output except pin0 as input
	PORTC = 0x01; // set all output pins to 0, and set pin0 (input) to pull-up
#endif /* EMU */

#define LINE_LEN 40

	char input_buf[LINE_LEN];
	char* p_buf = input_buf;

#ifndef EMU	
	uart_init();
#endif /* EMU */
	eeprom_init_spi();

	for (;;) {
		putchar ('#');
		putchar (' ');
 		read_line (input_buf, sizeof (input_buf) - 1, stdin);
		if (strnlen (input_buf, LINE_LEN) == 0)
			continue;
		parse_cmd_line (&p_buf);

#ifndef EMU
		if (!was_set) {
			PORTC |= _BV(1);
			PORTC &= ~(_BV(2));
			was_set = 1;
		}
		else {
			PORTC |= _BV(2);
			PORTC &= ~(_BV(1));
			was_set = 0;
		}
#endif /* EMU */
	}
	return 0;
}
