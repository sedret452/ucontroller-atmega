/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * 23k256_emu_lib.c: SPI access module for SRAM chip (Microchip 23K256), emulated
 *
 * Copyright	(C) 2013-2022 sedret452
 */


/*
  TODO:

   - How to emulate status register properly?  I mean there's no
     emulation of clock behavior of whatsoever!  But proper page
     boundary wraps should be emulated through status register!
     */

#include "23k256_lib.h"
#include <stdlib.h>

const uint8_t  page_size = 32;   // 32 bytes
const uint16_t max_page  = 1023; // 0 to 1023 pages

static struct {
	/* uint8_t memory[page_size * (max_page + 1)]; */
	uint8_t* memory;
	uint8_t status_reg;
} __memory_emu;


void sram_init_spi ()
{
	// Be aware, I originally wanted to use static allocation for
	// 'memory' this wasn't possible due to a avr-gcc constraint.  It
	// will limit an array to a maximum element size of:
	//
	//   (32 * 1024) - 1
	__memory_emu.memory = (uint8_t*) malloc (page_size * (max_page + 1));
	__memory_emu.status_reg = 0;

	srand (42); // always use 42 as seed!
	for (int i = 0; i < page_size * max_page; i++) {
		__memory_emu.memory[i] = rand() % 256;
	}
}

sram_op_mode sram_read_status_register ()
{
	return __memory_emu.status_reg;
}

int sram_write_status_register (sram_op_mode mode)
{
	__memory_emu.status_reg = mode;
	return 0;
}
	
uint8_t sram_read_byte (sram_mem_address_t address)
{
	return __memory_emu.memory[address];
}

int sram_write_byte (sram_mem_address_t address, uint8_t val)
{
	__memory_emu.memory[address] = val;
}

int sram_read_page (uint8_t* val_array, uint8_t page, uint8_t addr_in_page)
{
	for ( ; addr_in_page < page_size; addr_in_page++) {
		val_array[addr_in_page] = __memory_emu.memory[(page * page_size) + addr_in_page];
	}
	return 0;
}

int sram_write_page (uint8_t page, uint8_t* val_array, uint8_t addr_in_page)
{
	for ( ; addr_in_page < page_size; addr_in_page++) {
		__memory_emu.memory[(page * page_size) + addr_in_page] = val_array[addr_in_page];
	}

	return -1;
}

int sram_read_sequential (uint8_t* val_vec, uint16_t len, uint8_t page, uint8_t addr_in_page)
{
	for (int i = 0; i < len; i++) {
		val_vec[i] = __memory_emu.memory [(page * page_size) + addr_in_page + i];
	}
	return 0;
}

int sram_write_sequential (uint8_t page, uint8_t addr_in_page, uint8_t* val_vec, uint16_t len)
{
	for (int i = 0; i < len; i++) {
		__memory_emu.memory[(page * page_size) + addr_in_page + i] = val_vec[i];
	}
	return -1;
}
