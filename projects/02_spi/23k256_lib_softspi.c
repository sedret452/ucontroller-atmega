/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * 23k256_lib_softspi.h: (soft) SPI access module for SRAM chip (Microchip 23K256)
 *
 *  Soft SPI: Implement SPI-handling via software on any I/O pin,
 *            instead of hardware-supported SPI pins.
 *
 * Copyright	(C) 2013-2022 sedret452
 */

#include "23k256_lib.h"

#include <avr/io.h>
#include "uart_lib.h"
#include "log_lib.h"

const uint8_t  page_size = 32;   // 32 bytes
const uint16_t max_page  = 1023; // 0 to 1023 pages


#define PIN_SCK  PB5
#define PIN_MISO PB4
#define PIN_MOSI PB3
#define PIN_SS   PB2

inline static void spi_soft_ss_low (void) __attribute__((always_inline));
inline static void spi_soft_ss_high (void) __attribute__((always_inline));
inline static void spi_soft_sck_low (void) __attribute__((always_inline));
inline static void spi_soft_sck_high (void) __attribute__((always_inline));
inline static void spi_soft_mosi_low (void) __attribute__((always_inline));
inline static void spi_soft_mosi_high (void) __attribute__((always_inline));

inline static void
spi_soft_ss_low(void)
{
	PORTB &= ~(1<<PORTB2);
}

inline static void
spi_soft_ss_high(void)
{
	PORTB |= (1<<PORTB2);
}

inline static void
spi_soft_sck_high (void)
{
	PORTB |= (1<<PORTB5);
}

inline static void
spi_soft_sck_low (void)
{
	PORTB &= ~(1<<PORTB5);
}

inline static void
spi_soft_mosi_high (void)
{
	PORTB |= (1<<PORTB3);
}

inline static void
spi_soft_mosi_low (void)
{
	PORTB &= ~(1<<PORTB3);
}

static uint8_t
spi_soft_recv_8 ()
{
	uint8_t val = 0;
	uint8_t cur = 0;
	for (uint8_t i = 1; i <= 8; i++) {
		val = val << 1;
		spi_soft_sck_high ();
		cur = bit_is_set (PINB, PINB4);
		spi_soft_sck_low ();
		log_info ("spi_soft_recv_8(): cur = 0x%02x\n", cur);
		if (cur) {
			val++;
		}
		cur = 0;
	}
	return val;
}

static uint16_t
spi_soft_recv (uint8_t nbits)
{
	uint16_t val = 0;
	
	for (uint8_t i = 1; i <= nbits; i++) {
		val = val << 1;
		spi_soft_sck_high ();
 		if (bit_is_set (PINB, PINB4)) {
			val++;
		}
		spi_soft_sck_low ();
	}
	log_debug ("spi_soft_recv(): received 0x%04x\n", val);
	return val;
}

void
spi_soft_send_8 (uint8_t val)
{
	uint8_t _t = val;
	
	for (uint8_t i = 1; i <= 8; i++) {
		if (val & 0x80) {
			spi_soft_mosi_high ();
		}
		else {
			spi_soft_mosi_low ();
		}
		spi_soft_sck_high ();
		__builtin_avr_delay_cycles(2);
		spi_soft_sck_low ();
		val = val << 1;
	}

	log_info_1 ("spi_soft_send_8(): sequence:  ");
	for (uint8_t i = 1; i <= 8; i++) {
		if (_t & 0x80) {
			printf ("1");
		}
		else {
			printf ("0");
		}
		_t = _t << 1;
	}
	printf ("\n");
}

void
spi_soft_send (uint8_t nbits, uint16_t val)
{
	log_debug ("spi_soft_send(): before corection 0x%04x\n", val);
	val <<= (16 - nbits); // adjust val to MSB
	log_debug ("spi_soft_send(): after corection 0x%04x\n", val);
	
	for (uint8_t i = 1; i <= nbits; i++) {
		if (val & 0x8000) {
			spi_soft_mosi_high ();
		}
		else {
			spi_soft_mosi_low ();
		}
		spi_soft_sck_high ();
		__builtin_avr_delay_cycles (2);
		spi_soft_sck_low ();
		val <<= 1;
	}
}

void
disable_spi ()
{
	SPCR = 0;
	
	uint8_t __attribute__ ((unused)) clear = 0;
	clear = SPSR;
	clear = SPDR;

}

void
enable_spi ()
{
	// Enable SPI, as master and as SPI mode 0 (CPOL = 0, CPHA = 0)
	SPCR = (1 << SPE) | (1 << MSTR);// | (1 << CPOL) | (1 << CPHA);

	uint8_t __attribute__ ((unused)) clear = 0;
	clear = SPSR;
	clear = SPDR;
}


void
sram_init_spi ()
{
	// Configure MOSI, SCK and SS for output
	DDRB = (1 << PB3) | (1 << PB5) | (1 << PB2); // MOSI, SCK, SS

	// Enable SPI, as master and as SPI mode 0 (CPOL = 0, CPHA = 0)
	/* SPCR = (1 << SPE) | (1 << MSTR);// | (1 << CPOL) | (1 << CPHA); */

	/* uint8_t __attribute__ ((unused)) clear = 0; */
	/* clear = SPSR; */
	/* clear = SPDR; */
}

sram_op_mode
sram_read_status_register ()
{
	sram_op_mode mode = RESERVED_MODE;
	uint8_t _mode = 0;

	spi_soft_ss_low  ();
	spi_soft_send (8, RDSR);
	_mode = spi_soft_recv (8);
	spi_soft_ss_high ();
	mode = _mode;
	
	return mode; // TODO
}

int
sram_write_status_register (sram_op_mode mode)
{
	log_debug ("trying to write following mode: 0x%x\n", mode);

	spi_soft_ss_low();
	spi_soft_send (8, WRSR);
	spi_soft_send (8, mode);
	spi_soft_ss_high();

	return mode; // TODO
}

uint8_t
sram_read_byte (sram_mem_address_t address)
{
	unsigned char _val = 0;

	spi_soft_ss_low ();
	spi_soft_send (8, READ);
	spi_soft_send (16, address);
	_val = spi_soft_recv (8);
	spi_soft_ss_high ();

	log_info ("sram_read_byte(): _val: 0x%x\n", _val);

	return (uint8_t) _val;
}

int
sram_write_byte (sram_mem_address_t address, uint8_t val)
{
	spi_soft_ss_low ();
	spi_soft_send (8, WRITE);
	spi_soft_send (16, address);
	spi_soft_send (8, val);
	spi_soft_ss_high ();
	
	return val;
}

int
sram_read_page (uint8_t* val_array, uint8_t page, uint8_t addr_in_page)
{
	log_info_1 ("sram_read_page(): ignored\n");
	return -1;
}

int
sram_write_page (uint8_t page, uint8_t* val_array, uint8_t addr_in_page)
{
	log_info_1 ("sram_write_page(): ignored\n");
	return -1;
}

int
sram_read_sequential (uint8_t* val_vec, uint16_t len, uint8_t page, uint8_t addr_in_page)
{
	log_info_1 ("sram_read_sequential(): ignored\n");
	return -1;
}

int
sram_write_sequential (uint8_t page, uint8_t addr_in_page, uint8_t* val_vec, uint16_t len)
{
	log_info_1 ("sram_write_sequential(): ignored\n");
	return -1;
}
